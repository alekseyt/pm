#ifndef PM_DETAIL_CLAUSE_HPP
#define PM_DETAIL_CLAUSE_HPP

#include <concepts>
#include <optional>
#include <type_traits>

namespace pm { namespace detail {

// `Passable` flag means that clause does not contribute to
// return value of match expression.
//
// 5.2 Basic Model
// If ! prefix is used before compound statement - the statement would not contribute
// to return type deduction for inspect expression. Such a statement is not expected
// to yield a value and should stop the execution either by returning from the enclosing
// function, throwing an exception or terminating the program.
template <typename Pattern, typename Callable, bool Passable>
struct Clause
{
	using passable_t = std::bool_constant<Passable>;
	using pattern_t = Pattern;
	using callback_t = Callable;

	pattern_t pattern;
	callback_t callback;
};

struct EvalResultVoid
{
	using value_t = void;
	using result_t = void;

	bool matched = false;
};

template <typename V>
struct EvalResultValue : EvalResultVoid
{
	using value_t = V;
	using result_t = std::optional<V>;

	result_t result;
};

template <typename T>
concept is_eval_result = std::same_as<EvalResultVoid, T>
|| requires (T t) {
	{ detail::EvalResultValue{t} } -> std::same_as<T>;
};

static_assert(detail::is_eval_result<EvalResultVoid>);
static_assert(detail::is_eval_result<EvalResultValue<int>>);
static_assert(detail::is_eval_result<EvalResultValue<bool>>);

template <typename Clause, typename V, bool Strict>
concept is_clause = requires (Clause c) {
	typename Clause::passable_t;
	typename Clause::pattern_t;
	typename Clause::callback_t;
	c.pattern;
	c.callback;
} && requires (Clause::passable_t p) {
	{ std::bool_constant{p} } -> std::same_as<typename Clause::passable_t>;
} && requires (Clause c, V v) {
	{ c.template evaluate<Strict>(v) } -> detail::is_eval_result;
};

}} // namespace detail // namespace pm

#endif // PM_DETAIL_CLAUSE_HPP

#ifndef PM_DETAIL_MATCHER_HPP
#define PM_DETAIL_MATCHER_HPP

#include <concepts>

namespace pm { namespace detail {

/// Direction of matching should be "pattern matches value", not the other way around.
/// This way there is less ambiguity when overload is selected and a sort of hierarchy
/// can exist, e.g. variant matches values, any_of matches variant and not the other way
/// around when variant matches any_of since that would require variant to know about
/// any_of existence beforehand.
template <typename Pattern, typename V>
struct matcher;

template <typename Pattern, typename V, bool Strict=true,
	typename Matcher=detail::matcher<Pattern, V>>
concept has_matcher = requires (Matcher m, Pattern &&p, V &&v) {
	// matcher can always be instantiated (most generic one)
	// but these methods are not going to be provided by matcher
	// if it can't match Pattern with V
	{ m.template match<Strict>(p, v) } -> std::same_as<bool>;
	// both pattern and value are being passed to result() again
	// so matcher can extract matched value from nested matcher
	{ m.result(p, v) };
};

template <typename Pattern, typename V>
constexpr auto get_matcher() noexcept
{
	// use constructor instead of initialization
	// this produces more sensible error messages when something goes wrong
	return detail::matcher<std::decay_t<Pattern>, V>();
}

constexpr auto get_matcher(auto &&p, auto &&v) noexcept
{
	return get_matcher<decltype(p), decltype(v)>();
}

}} // namespace detail // namespace pm

#endif // PM_DETAIL_MATCHER_HPP

#ifndef PM_EXT_ANY_OF_RANGE_HPP
#define PM_EXT_ANY_OF_RANGE_HPP

#include <algorithm>
#include <cassert>
#include <iterator>
#include <optional>
#include <ranges>
#include <pm/detail/concepts.hpp>
#include <pm/detail/matcher.hpp>

namespace pm {
namespace detail {

/// any_of pattern for ranges
template <typename T>
	requires (std::ranges::range<T>)
struct any_of_range
{
	using explicit_variant_matching = std::true_type;

	using range_t = T;

	explicit constexpr any_of_range(const T &t)
	: range(t)
	{}

	const T &range;
};

template <typename T>
concept is_any_of_range = requires (T t) {
	{ pm::detail::any_of_range{t} } -> std::same_as<T>;
};

/// Same as match_any_of but on range and most likely at runtime
template <typename Range>
	requires (std::ranges::range<std::remove_cvref_t<Range>>)
constexpr auto match_any_of_range(auto &&v, const Range &r)
{
	return std::ranges::find_if(r, [&v] (const auto &x) {
		return detail::get_matcher(x, v).match(x, v);
	});
}

template <typename Pattern, typename V>
	requires (detail::is_any_of_range<std::remove_cvref_t<Pattern>>)
struct any_of_range_matcher
{
	using range_t = typename Pattern::range_t;
	using range_iterator_t = decltype(std::ranges::cbegin(std::declval<range_t&>()));
	using range_value_t = typename std::iterator_traits<range_iterator_t>::value_type;

	std::optional<range_iterator_t> matched;

	template <bool Strict=true>
	constexpr bool match(const Pattern &any_of, auto &&v) {
		const auto it = match_any_of_range(v, any_of.range);
		if (it == std::cend(any_of.range)) {
			return false;
		}

		matched = std::make_optional(it);
		return true;
	}

	constexpr decltype(auto) result([[maybe_unused]] const Pattern &p, [[maybe_unused]] auto &&v) const {
		assert(matched);
		return **matched;
	}
};

/// any_of_range -> value matcher
template <typename Pattern, typename V>
	requires (detail::is_any_of_range<std::remove_cvref_t<Pattern>>)
struct matcher<Pattern, V> : any_of_range_matcher<Pattern, V> {};

} // namespace detail

template <typename T>
	requires (std::ranges::range<std::remove_cvref_t<T>>)
constexpr detail::is_any_of_range auto any_of(const T &t)
{
	return detail::any_of_range(t);
}

} // namespace pm

#endif // PM_EXT_ANY_OF_RANGE_HPP

#include <cassert>
#include <string>
#include <variant>
#include <pm/ext/any.hpp>
#include <pm/ext/any_of.hpp>
#include <pm/pm.hpp>
#include "match.hpp"

// variant to value
static_assert(pm::detail::has_matcher<std::variant<int>, int>);
static_assert(pm::detail::has_matcher<std::variant<int>, int &>);
static_assert(pm::detail::has_matcher<std::variant<int>, const int &>);
static_assert(pm::detail::has_matcher<std::variant<int>, int &&>);
static_assert(pm::detail::has_matcher<int, std::variant<int>>);
static_assert(pm::detail::has_matcher<int &, std::variant<int>>);
static_assert(pm::detail::has_matcher<const int &, std::variant<int>>);
static_assert(pm::detail::has_matcher<int &&, std::variant<int>>);
static_assert(pm::detail::has_matcher<std::variant<int>, float>);
static_assert(pm::detail::has_matcher<float, std::variant<int>>);
// variant to type
static_assert(pm::detail::has_matcher<std::variant<int>, pm::Type<int>>);
static_assert(pm::detail::has_matcher<std::variant<int>, pm::Type<float>>);
static_assert(pm::detail::has_matcher<pm::Type<int>, std::variant<int>>);
static_assert(pm::detail::has_matcher<pm::Type<float>, std::variant<int>>);
// variant to variant
static_assert(pm::detail::has_matcher<std::variant<int>, std::variant<int>>);
static_assert(pm::detail::has_matcher<std::variant<float>, std::variant<int>>);
static_assert(pm::detail::has_matcher<std::variant<float, int>, std::variant<int, float>>);

// Low-level matching
void test_variant_to_values()
{
	std::variant<std::string, int> v = 10;
	assert(pm::detail::match(v, 10));
	assert(!pm::detail::match(v, "abc"));

	v = "abc";
	assert(pm::detail::match(v, "abc"));
	assert(!pm::detail::match(v, 10));

	// reverse order of matching: value to variant
	v = 11;
	assert(pm::detail::match(11, v));
	assert(!pm::detail::match("abc", v));

	v = "cba";
	assert(pm::detail::match("cba", v));
	assert(!pm::detail::match(11, v));
}

void test_variant_to_types()
{
	std::variant<std::string, int> v = 10;
	assert(pm::detail::match(v, pm::type(int{})));
	assert(!pm::detail::match(v, pm::type(float{})));
	assert(!pm::detail::match(v, pm::type(bool{})));
	assert(!pm::detail::match(v, pm::type(std::string{})));

	v = "abc";
	assert(pm::detail::match(v, pm::type(std::string{})));
	assert(!pm::detail::match(v, pm::type<const char*>{}));
	assert(!pm::detail::match(v, pm::type(int{})));

	// reverse order of matching: type to variant
	v = 11;
	assert(pm::detail::match(pm::type(int{}), v));
	assert(!pm::detail::match(pm::type(std::string{}), v));

	v = "cba";
	assert(pm::detail::match(pm::type(std::string{}), v));
	assert(!pm::detail::match(pm::type(int{}), v));
}

void test_variant_to_variant()
{
	std::variant<std::string, int> v1 = 10;
	std::variant<int, bool> v2 = 10;

	assert(pm::detail::match(v1, v2));

	std::tie(v1, v2) = std::make_tuple(10, 11);
	assert(!pm::detail::match(v1, v2));

	std::tie(v1, v2) = std::make_tuple(1, true);
	assert(!pm::detail::match(v1, v2));
}

// High-level matching
void test_variant_visiting()
{
	const auto visitor = [] (auto &&arg) {
		const std::optional<int> ret = pm::match(pm::type(arg), pm::with(pm::type(int{}), [] {
			return 0;
		}) | pm::with(pm::type(std::string{}), [] {
			return 1;
		}));

		assert(ret);
		return *ret;
	};

	std::variant<std::string, int> v = 10;
	assert(std::visit(visitor, v) == 0);

	v = "abc";
	assert(std::visit(visitor, v) == 1);
}

void test_variant_match()
{
	std::variant<std::string, int> v = 10;
	assert(pm::match(v
		| pm::with(10, [] { return true; })));
	assert(!pm::match(v
		| pm::with(11, [] { return true; })));

	v = "abc";
	assert(pm::match(v
		| pm::with("abc", [] { return true; })));
	assert(!pm::match(v
		| pm::with("cba", [] { return true; })));
}

void test_variant_match_mixed_type_value()
{
	std::variant<std::string, int> v = 10;
	assert(pm::match(std::tie(v, v), pm::with(std::make_tuple(pm::type(int{}), 10), [] {
		return true;
	})));
	assert(!pm::match(std::tie(v, v), pm::with(std::make_tuple(pm::type(int{}), 11), [] {
		return true;
	})));
	assert(!pm::match(std::tie(v, v), pm::with(std::make_tuple(pm::type(std::string{}), 10), [] {
		return true;
	})));
	assert(!pm::match(std::tie(v, v), pm::with(std::make_tuple(pm::type(std::string{}), 11), [] {
		return true;
	})));
}

void test_variant_match_any()
{
	std::variant<int, std::string> v;
	assert(pm::match(v, pm::with(pm::__, [] {
		return true;
	})));
}

void test_variant_strict()
{
	std::variant<std::string> v;

	// normally should produce compile error, but not with Strict=false
	assert(!pm::match<false>(v
		| pm::with(10, [] { return true; })));
}

#ifndef PM_ADAPTOR_HPP
#define PM_ADAPTOR_HPP

#include <concepts>
#include <tuple>
#include <pm/detail/adaptor.hpp>
#include <pm/detail/expression.hpp>

/// Adaptors compositing using pipe operator
template <typename AdaptorA, typename AdaptorB>
	requires (pm::detail::is_adaptor<AdaptorA>
	&& pm::detail::is_adaptor<AdaptorB>)
constexpr pm::detail::is_adaptor auto operator| (AdaptorA a, AdaptorB b) noexcept
{
	return pm::detail::make_adaptor(std::tuple_cat(a.clauses, b.clauses));
}

/// Pipe for whole expression
/// value + Adaptor -> Expression
template <typename V, typename Adaptor>
	requires (!(pm::detail::is_adaptor<V> || pm::detail::is_expression<V>)
	&& pm::detail::is_adaptor<Adaptor>)
constexpr pm::detail::is_expression auto operator| (V &&v, Adaptor &&a) noexcept
{
	return pm::detail::make_expression(std::forward<V>(v), std::forward<Adaptor>(a));
}

/// Expression + Adaptor -> Expression
template <typename Expression, typename Adaptor>
	requires (pm::detail::is_expression<Expression>
	&& pm::detail::is_adaptor<Adaptor>)
constexpr pm::detail::is_expression auto operator| (Expression &&e, Adaptor &&a) noexcept
{
	return pm::detail::make_expression(e.value,
		pm::detail::make_adaptor(std::tuple_cat(e.adaptor.clauses, a.clauses)));
}

#endif // PM_ADAPTOR_HPP

#include <cassert>
#include <string>
#include <pm/ext/within.hpp>
#include <pm/pm.hpp>

static_assert(pm::detail::has_matcher<pm::detail::within<int, int>, int>);
static_assert(pm::detail::has_matcher<pm::detail::within<int, int>, int &>);
static_assert(pm::detail::has_matcher<pm::detail::within<int, int>, const int &>);
static_assert(pm::detail::has_matcher<pm::detail::within<int, int>, int &&>);
static_assert(pm::detail::has_matcher<pm::detail::within<int, int>, float>);

void test_within()
{
	// ints
	assert(pm::match(10, pm::with(pm::within(9, 11), [] {
		return true;
	})));
	assert(pm::match(9, pm::with(pm::within(9, 11), [] {
		return true;
	})));
	assert(pm::match(11, pm::with(pm::within(9, 11), [] {
		return true;
	})));
	assert(!pm::match(8, pm::with(pm::within(9, 11), [] {
		return true;
	})));
	assert(!pm::match(12, pm::with(pm::within(9, 11), [] {
		return true;
	})));

	// reversed range
	assert(pm::match(10, pm::with(pm::within(11, 9), [] {
		return true;
	})));

	// collapsed range
	assert(pm::match(12, pm::with(pm::within(12, 12), [] {
		return true;
	})));

	// float value, int args
	assert(pm::match(10.5f, pm::with(pm::within(10, 11), [] {
		return true;
	})));
	// mixed args
	assert(pm::match(11.2f, pm::with(pm::within(10, 11.5), [] {
		return true;
	})));
	assert(!pm::match(11.6f, pm::with(pm::within(10, 11.5), [] {
		return true;
	})));

	// chars
	assert(pm::match('b', pm::with(pm::within('a', 'c'), [] {
		return true;
	})));
	assert(!pm::match('d', pm::with(pm::within('a', 'b'), [] {
		return true;
	})));
	assert(!pm::match(11, pm::with(pm::within('a', 'b'), [] {
		return true;
	})));

	// strings
	assert(pm::match(std::string("b"), pm::with(pm::within(std::string("a"), std::string("c")), [] {
		return true;
	})));
	assert(!pm::match(std::string("d"), pm::with(pm::within(std::string("a"), std::string("c")), [] {
		return true;
	})));
}

void test_within_pair()
{
	const std::pair<int, float> p(9, 11.5);
	assert(pm::match(9, pm::with(pm::within(p), [] {
		return true;
	})));
	assert(pm::match(10, pm::with(pm::within(p), [] {
		return true;
	})));
	assert(pm::match(11, pm::with(pm::within(p), [] {
		return true;
	})));
	assert(pm::match(11.5, pm::with(pm::within(p), [] {
		return true;
	})));
	assert(!pm::match(8.9, pm::with(pm::within(p), [] {
		return true;
	})));
	assert(!pm::match(11.6, pm::with(pm::within(p), [] {
		return true;
	})));
}

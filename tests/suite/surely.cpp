#include <cassert>
#include <optional>
#include <string>
#include <type_traits>
#include <pm/ext.hpp>
#include <pm/pm.hpp>

static_assert(pm::detail::has_matcher<pm::detail::surely<int>, int *>);
static_assert(pm::detail::has_matcher<pm::detail::surely<int>, const int *>);
static_assert(pm::detail::has_matcher<pm::detail::surely<int>, int *&>);
static_assert(pm::detail::has_matcher<pm::detail::surely<float>, float *>);

void test_surely_pointer()
{
	int x = 10;
	int *px = &x;
	assert(pm::match(px
		| pm::with(pm::surely(10), [] { return true; })));

	x = 11;
	assert(!pm::match(px
		| pm::with(pm::surely(10), [] { return true; })));
}

void test_surely_optional()
{
	std::optional<int> o = 10;
	assert(pm::match(o
		| pm::with(pm::surely(10), [] { return true; })));
	assert(!pm::match(o
		| pm::with(pm::surely(11), [] { return true; })));
}

// Check that callback gets deferenced value
void test_surely_callback()
{
	std::optional<int> o = 11;
	const auto match = pm::match(o
		| pm::with(pm::surely(pm::__), [] (int x) { return x * 2; }));
	assert(match && *match == 22);
}

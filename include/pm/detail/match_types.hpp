#ifndef PM_DETAIL_MATCH_TYPES_HPP
#define PM_DETAIL_MATCH_TYPES_HPP

#include <cassert>
#include <concepts>
#include <pm/detail/matcher.hpp>
#include <pm/type.hpp>

namespace pm { namespace detail {

/// Match type with type
template <typename Pattern, typename V>
	requires (detail::is_type<std::remove_cvref_t<Pattern>>
	&& detail::is_type<std::remove_cvref_t<V>>)
struct matcher<Pattern, V>
{
	using pattern_t = typename std::remove_cvref_t<Pattern>;
	using value_t = typename std::remove_cvref_t<V>;
	using pattern_type_t = typename pattern_t::type_t;
	using value_type_t = typename value_t::type_t;

	bool matched = false;

	/// True if one type is same as another type
	template <bool Strict=true>
	constexpr bool match([[maybe_unused]] const Pattern &p, [[maybe_unused]] auto &&v) noexcept {
		if constexpr (std::same_as<pattern_type_t, value_type_t>) {
			matched = true;
		}

		return matched;
	}

	constexpr decltype(auto) result([[maybe_unused]] const Pattern &p, auto &&v) const noexcept {
		assert(matched);
		return std::forward<decltype(v)>(v);
	}
};

}} // namespace detail // namespace pm

#endif // PM_DETAIL_MATCH_TYPES_HPP

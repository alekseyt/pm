#include <cassert>
#include <string>
#include <pm/pm.hpp>
#include "match.hpp"

static_assert(pm::detail::has_matcher<int, int>);
static_assert(pm::detail::has_matcher<int, int&>);
static_assert(pm::detail::has_matcher<int, const int&>);
static_assert(pm::detail::has_matcher<int, int&&>);

static_assert(pm::detail::has_matcher<int[3], int[3]>);
static_assert(pm::detail::has_matcher<const char *, char[3]>);

// Low-level matching
void test_values_to_values()
{
	// ints
	assert(pm::detail::match(10, 10));
	assert(!pm::detail::match(10, -10));
	assert(pm::detail::match(-10, -10));
	assert(!pm::detail::match(-10, 10));

	// floats
	assert(pm::detail::match(10.0f, 10.0f));
	assert(pm::detail::match(10.0f, 10.0));
	assert(pm::detail::match(10.0f, 10));
	assert(!pm::detail::match(10.0f, 10.5f));

	// strings
	assert(pm::detail::match("abc", "abc"));
	assert(!pm::detail::match("abc", "cba"));
	assert(pm::detail::match("abc", std::string("abc")));
	assert(!pm::detail::match("abc", std::string("cba")));
	assert(pm::detail::match(std::string("abc"), "abc"));
	assert(!pm::detail::match(std::string("abc"), "cba"));
	assert(pm::detail::match(std::string("abc"), std::string("abc")));
	assert(!pm::detail::match(std::string("abc"), std::string("cba")));
}

// High-level matching
void test_values_match()
{
	assert(pm::match(10, pm::with(10, [] {
		return true;
	})));
	assert(!pm::match(10, pm::with(11, [] {
		return true;
	})));
}

void test_values_strict()
{
	// normally should produce compile error, but not with Strict=false
	assert(!pm::match<false>("abc"
		| pm::with(10, [] { return true; })));
}

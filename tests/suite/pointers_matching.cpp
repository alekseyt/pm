#include <cassert>
#include <memory>
#include <pm/pm.hpp>
#include "match.hpp"

static_assert(pm::detail::has_matcher<int *, int *>);
static_assert(pm::detail::has_matcher<int *, int *&>);
static_assert(pm::detail::has_matcher<int *, const int *>);

static_assert(pm::detail::has_matcher<int *, std::nullptr_t>);
static_assert(pm::detail::has_matcher<const char *, const char *>);
static_assert(pm::detail::has_matcher<const char *, std::nullptr_t>);

static_assert(pm::detail::has_matcher<std::nullptr_t, std::unique_ptr<int>>);
static_assert(pm::detail::has_matcher<int *, std::unique_ptr<int>>);

void test_pointers_to_pointers()
{
	assert(pm::detail::match(nullptr, nullptr));

	int x = 0, *npx = nullptr, *px = &x;
	assert(pm::detail::match(nullptr, npx));
	assert(!pm::detail::match(nullptr, &x));
	assert(!pm::detail::match(npx, &x));
	assert(pm::detail::match(px, &x));
}

void test_pointers_to_smart_pointers()
{
	std::unique_ptr<int> upx = std::make_unique<int>(10);
	int *raw_upx = upx.get();

	assert(!pm::detail::match(upx, nullptr));
	assert(pm::detail::match(upx, raw_upx));

	// reverse order
	assert(!pm::detail::match(nullptr, upx));
	assert(pm::detail::match(raw_upx, upx));

	int x = 0, *px = &x;
	assert(!pm::detail::match(upx, px));

	upx.reset();
	assert(pm::detail::match(upx, nullptr));
}

void test_pointers_match()
{
	std::unique_ptr<int> unique_ptr;

	assert(pm::match(unique_ptr
		| pm::with(nullptr, [] { return true; })));

	unique_ptr = std::make_unique<int>(10);
	assert(!pm::match(unique_ptr
		| pm::with(nullptr, [] { return true; })));
	assert(pm::match(unique_ptr
		| pm::with(unique_ptr.get(), [] { return true; })));

	std::shared_ptr<int> shared_ptr;
	assert(pm::match(shared_ptr
		| pm::with(nullptr, [] { return true; })));

	shared_ptr = std::make_shared<int>(10);
	assert(!pm::match(unique_ptr
		| pm::with(nullptr, [] { return true; })));
	assert(pm::match(unique_ptr
		| pm::with(unique_ptr.get(), [] { return true; })));
}

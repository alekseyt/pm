#include <cassert>
#include <string>
#include <variant>
#include <pm/ext/any_of.hpp>
#include <pm/ext/any_of_range.hpp>
#include <pm/pm.hpp>

// any_of to value
static_assert(pm::detail::has_matcher<pm::detail::any_of<int>, int>);
static_assert(pm::detail::has_matcher<pm::detail::any_of<int>, int &>);
static_assert(pm::detail::has_matcher<pm::detail::any_of<int>, const int &>);
static_assert(pm::detail::has_matcher<pm::detail::any_of<int>, int &&>);
static_assert(pm::detail::has_matcher<pm::detail::any_of<int, int>, int>);
// any_of to variant
static_assert(pm::detail::has_matcher<pm::detail::any_of<int>, std::variant<int>>);
static_assert(pm::detail::has_matcher<pm::detail::any_of<int, int>, std::variant<float>>);
// any_of_range
static_assert(pm::detail::has_matcher<pm::detail::any_of_range<int[3]>, int>);
static_assert(pm::detail::has_matcher<pm::detail::any_of_range<int[3]>, std::variant<int, float>>);

void test_any_of()
{
	assert(pm::match(9, pm::with(pm::any_of(9, 10, 11), [] {
		return true;
	})));
	assert(pm::match(10, pm::with(pm::any_of(9, 10, 11), [] {
		return true;
	})));
	assert(pm::match(11, pm::with(pm::any_of(9, 10, 11), [] {
		return true;
	})));
	assert(!pm::match(8, pm::with(pm::any_of(9, 10, 11), [] {
		return true;
	})));
	assert(!pm::match(12, pm::with(pm::any_of(9, 10, 11), [] {
		return true;
	})));
}

void test_any_of_singular()
{
	assert(pm::match(9, pm::with(pm::any_of(9), [] {
		return true;
	})));
	assert(!pm::match(10, pm::with(pm::any_of(9), [] {
		return true;
	})));
}

void test_any_of_variant_values()
{
	std::variant<int, std::string> v = 10;
	assert(pm::match(v, pm::with(pm::any_of(10, "abc"), [] {
		return true;
	})));

	v = "abc";
	assert(pm::match(v, pm::with(pm::any_of(10, "abc"), [] {
		return true;
	})));

	v = 11;
	assert(!pm::match(v, pm::with(pm::any_of(10, "abc"), [] {
		return true;
	})));

	v = "bca";
	assert(!pm::match(v, pm::with(pm::any_of(10, "abc"), [] {
		return true;
	})));

	v = 10;
	assert(!pm::match(v, pm::with(pm::any_of('a', 'b'), [] {
		return true;
	})));
}

void test_any_of_variant_types()
{
	std::variant<int, std::string, bool> v = 10;
	assert(pm::match(v, pm::with(pm::any_of(pm::type(int{}), pm::type(std::string{})), [] {
		return true;
	})));

	v = "abc";
	assert(pm::match(v, pm::with(pm::any_of(pm::type(int{}), pm::type(std::string{})), [] {
		return true;
	})));

	v = true;
	assert(!pm::match(v, pm::with(pm::any_of(pm::type(int{}), pm::type(std::string{})), [] {
		return true;
	})));
	assert(pm::match(v, pm::with(pm::any_of(pm::type(int{}), pm::type(bool{})), [] {
		return true;
	})));
}

void test_any_of_range()
{
	const int range[3] = { 9, 10, 11 };
	assert(pm::match(10, pm::with(pm::any_of(range), [] {
		return true;
	})));
	assert(!pm::match(8.5, pm::with(pm::any_of(range), [] {
		return true;
	})));

	std::variant<int, std::string> v = 10;
	const auto match = pm::match(v, pm::with(pm::any_of(range), [] (int x) {
		return x * 2;
	}));
	assert(match && *match == 10 * 2);
}

void test_any_of_strict()
{
	// normally should produce compile error, but not with Strict=false
	assert(!pm::match<false>("abc"
		| pm::with(pm::any_of(9, 10, 11), [] { return true; })));

	// same for pass()
	bool pass_called = false;
	pm::match<false>("abc"
		| pm::pass(pm::any_of(9, 10, 11), [&] { pass_called = true; }));
	assert(!pass_called);
}

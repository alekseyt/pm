#include <cassert>
#include <memory>
#include <optional>
#include <pm/ext.hpp>
#include <pm/pm.hpp>

static_assert(pm::detail::has_matcher<pm::detail::maybe<int>, int *>);
static_assert(pm::detail::has_matcher<pm::detail::maybe<int>, const int *>);
static_assert(pm::detail::has_matcher<pm::detail::maybe<int>, int *&>);
static_assert(pm::detail::has_matcher<pm::detail::maybe<float>, float *>);

void test_maybe_optional()
{
	std::optional<int> o;
	assert(!pm::match(o
		| pm::with(pm::maybe(10), [] { return true; })));

	o = 10;
	assert(pm::match(o
		| pm::with(pm::maybe(10), [] { return true; })));

	o = 11;
	assert(!pm::match(o
		| pm::with(pm::maybe(10), [] { return true; })));
}

void test_maybe_shared_ptr()
{
	std::shared_ptr<int> sp;
	assert(!pm::match(sp
		| pm::with(pm::maybe(10), [] { return true; })));

	sp = std::make_shared<int>(10);
	assert(pm::match(sp
		| pm::with(pm::maybe(10), [] { return true; })));

	sp.reset();
	assert(!pm::match(sp
		| pm::with(pm::maybe(10), [] { return true; })));
}

// Check that callback gets deferenced value
void test_maybe_callback()
{
	std::optional<int> o = 11;
	const auto match = pm::match(o
		| pm::with(pm::maybe(pm::__), [] (int x) { return x * 2; }));
	assert(match && *match == 22);
}

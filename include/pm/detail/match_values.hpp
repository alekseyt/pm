#ifndef PM_DETAIL_MATCH_VALUES_HPP
#define PM_DETAIL_MATCH_VALUES_HPP

#include <cassert>
#include <pm/detail/matcher.hpp>

namespace pm { namespace detail {

/// Generic implementation to compare any value to any other value using operator==
template <typename Pattern, typename V>
struct matcher
{
	bool matched = false;

	/// Strict might be specialization-specific and normally means additional checks
	/// that matcher may perform to decide if interface has to be provided for Pattern/V
	template <bool Strict=true>
	constexpr bool match(const Pattern &p, auto &&v)
		// require this on match() only, but still provide result()
		// this way there are less error messages (and shorter code)
		requires (!Strict || std::equality_comparable_with<Pattern, V>)
	{
		if constexpr (std::equality_comparable_with<Pattern, V>) {
			matched = (v == p);
		}

		return matched;
	}

	constexpr decltype(auto) result([[maybe_unused]] const Pattern &p, auto &&v) const noexcept {
		assert(matched);
		return std::forward<decltype(v)>(v);
	}
};

}} // namespace detail // namespace pm

#endif // PM_DETAIL_MATCH_VALUES_HPP

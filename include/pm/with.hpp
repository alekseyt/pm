#ifndef PM_WITH_HPP
#define PM_WITH_HPP

#include <concepts>
#include <tuple>
#include <pm/detail/adaptor.hpp>
#include <pm/detail/with.hpp>

namespace pm {

/// Adaptor constructor
template <typename Pattern, typename Callable>
constexpr detail::is_adaptor auto with(Pattern p, Callable c) noexcept
{
	// wrap record into tuple, so when adaptors are joined
	// result remains a tuple of records
	return detail::make_adaptor(std::make_tuple(detail::with(p, c)));
}

template <typename Pattern, typename Callable, typename ConditionCheck>
constexpr detail::is_adaptor auto with(Pattern p, ConditionCheck check, Callable c) noexcept
{
	return detail::make_adaptor(std::make_tuple(detail::with<Pattern, Callable, check>(p, c)));
}

} // namespace pm

#endif // PM_WITH_HPP

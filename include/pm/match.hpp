#ifndef PM_MATCH_HPP
#define PM_MATCH_HPP

#include <concepts>
#include <optional>
#include <tuple>
#include <pm/detail/adaptor.hpp>
#include <pm/detail/clause.hpp>
#include <pm/detail/expression.hpp>
#include <pm/detail/match.hpp>

namespace pm {
namespace detail {

template <bool Strict, typename Clause, typename V>
	requires (detail::is_clause<Clause, V, Strict>)
constexpr detail::is_eval_result auto evaluate_clause(const Clause &clause, V &&v)
{
	return clause.template evaluate<Strict>(std::forward<V>(v));
}

/// Match (adaptor) conditions one by one, invoke callback and return if anything matched
template <bool Strict, std::size_t I, typename PrevRetT, typename V, typename Clauses>
	requires (I < std::tuple_size_v<Clauses>)
constexpr auto evaluate_expression(V &&v, const Clauses &clauses)
{
	using clause_passable_t = typename std::tuple_element_t<I, Clauses>::passable_t;

	const detail::is_eval_result auto eval_result =
		evaluate_clause<Strict>(std::get<I>(clauses), std::forward<V>(v));

	// dispatch result type, std::optional<V> for callbacks returning non-void
	using dispatch_result_t = typename decltype(eval_result)::result_t;
	// dispatch value type, V from std::optional<V> or void
	using dispatch_value_t = typename decltype(eval_result)::value_t;

	if (eval_result.matched) {
		// when clause's return type isn't void: return it
		if constexpr (!std::same_as<std::true_type, clause_passable_t>) {
			if constexpr (!std::same_as<void, dispatch_result_t>) {
				return eval_result.result;
			}
			else  {
				return;
			}
		}
	}

	if constexpr (I+1 < std::tuple_size_v<Clauses>) {
		return evaluate_expression<Strict, I+1, dispatch_value_t>(v, clauses);
	}

	// if it's a pass condition and previous return type wasn't void
	// return nullopt "in previous return type" so chain of returns
	// is maintained. example chain of calls:
	//
	// chain:
	//   pass() -> with() => void -> optional
	// translates into:
	//   return evaluate_expression { return optional }
	//
	// chain:
	//   with() -> pass() => optional -> optional
	// translates into:
	//   return evaluate_expression { return optional }
	//
	// chain:
	//   pass() -> pass() => void -> void
	// translates into:
	//   return evaluate_expression { return }
	//
	// in other words, pass() that follows with() inherits return type
	// and with() that follows pass() overwrites return type, but doesn't
	// overwrite return type if it's following another with()
	//
	// this makes compiler happy because all paths return same type
	// or unhappy if two with()s return different types
	if constexpr (std::same_as<std::true_type, clause_passable_t>
	&& !std::same_as<void, PrevRetT>) {
		return std::optional<PrevRetT>(std::nullopt);
	}
	// otherwise if eval result type isn't of void type, return its result
	else if constexpr (!std::same_as<void, dispatch_result_t>) {
		return eval_result.result;
	}
}

} // namespace detail

/// Match value to conditions (logical-or)
template <bool Strict=true, typename V, typename AdaptorT>
	requires (detail::is_adaptor<AdaptorT>)
constexpr auto match(V &&v, const AdaptorT &a)
{
	// start from void PrevRetT, with() that returns value will override it, pass() won't
	return detail::evaluate_expression<Strict, 0, void>(std::forward<V>(v), a.clauses);
}

template <bool Strict=true, typename Expression>
	requires (detail::is_expression<Expression>)
constexpr auto match(Expression &&expr)
{
	return match<Strict>(std::forward<decltype(expr.value)>(expr.value), expr.adaptor);
}

} // namespace pm

#endif // PM_MATCH_HPP

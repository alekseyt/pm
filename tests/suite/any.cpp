#include <cassert>
#include <tuple>
#include <pm/ext/any.hpp>
#include <pm/pm.hpp>

// any to value
static_assert(pm::detail::has_matcher<pm::detail::any, int>);
static_assert(pm::detail::has_matcher<pm::detail::any, float>);
// any to any
static_assert(pm::detail::has_matcher<pm::detail::any, pm::detail::any>);
// any to variant
static_assert(pm::detail::has_matcher<pm::detail::any, std::variant<int>>);
static_assert(pm::detail::has_matcher<pm::detail::any, std::variant<int, float>>);

void test_any()
{
	assert(pm::match(9
		| pm::with(pm::any(), [] { return true; })));
	assert(pm::match(10
		| pm::with(pm::any(), [] { return true; })));

	const auto match = pm::match(10
		| pm::with(11, [] { return false; })
		| pm::with(pm::any(), [] { return true; }));
	assert(match && *match);
}

void test_any_wildcard()
{
	const auto match = pm::match(10
		| pm::with(11, [] { return false; })
		| pm::with(pm::__, [] { return true; }));
	assert(match && *match);

	const int x = 10;
	assert(pm::match(std::make_tuple(x, 11)
		| pm::with(std::make_tuple(10, 11), [] { return true; })));
	assert(pm::match(std::make_tuple(x, 12)
		| pm::with(std::make_tuple(10, pm::__), [] { return true; })));
	assert(!pm::match(std::make_tuple(x, 11)
		| pm::with(std::make_tuple(11, pm::__), [] { return true; })));
	assert(pm::match(std::make_tuple(12, x)
		| pm::with(std::make_tuple(pm::__, 10), [] { return true; })));
}

void test_any_with_any()
{
	assert(pm::match(pm::__
		| pm::with(pm::__, [] { return true; })));
}

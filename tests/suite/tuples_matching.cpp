#include <cassert>
#include <string>
#include <tuple>
#include <pm/pm.hpp>
#include "match.hpp"

static_assert(pm::detail::has_matcher<std::tuple<int>, std::tuple<int>>);
static_assert(pm::detail::has_matcher<std::tuple<int>, std::tuple<int&>>);
static_assert(pm::detail::has_matcher<std::tuple<int>, std::tuple<const int &>>);
static_assert(pm::detail::has_matcher<std::tuple<int>, std::tuple<int&&>>);
static_assert(pm::detail::has_matcher<std::tuple<int, int>, std::tuple<int, int>>);
// can match tuples of different types (potentially -> false)
static_assert(pm::detail::has_matcher<std::tuple<int>, std::tuple<float>>);
// can't match tuples of different size
static_assert(!pm::detail::has_matcher<std::tuple<int>, std::tuple<int, float>>);

// Low-level matching
void test_tuple_to_tuple()
{
	assert(pm::detail::match(std::make_tuple(1, 2.0, "abc"), std::make_tuple(1, 2, std::string("abc"))));

	// nested tuple + tie
	const int x = 1;
	const std::string s = "abc";
	const std::tuple<float, std::string> t = { 2, s };
	assert(pm::detail::match(std::tie(x, t), std::make_tuple(1, std::make_tuple(2, "abc"))));
	assert(!pm::detail::match(std::tie(x, t), std::make_tuple(1, std::make_tuple(2, "cba"))));

	// tie
	assert(pm::detail::match(std::tie(x, s), std::make_tuple(1, "abc")));
	assert(!pm::detail::match(std::tie(x, s), std::make_tuple(1, "cba")));
	assert(!pm::detail::match(std::tie(x, s), std::make_tuple(2, "abc")));
}

// High-level matching
void test_tuples_match()
{
	assert(pm::match(std::make_tuple(10, 11), pm::with(std::make_tuple(10, 11), [] {
		return true;
	})));
	assert(!pm::match(std::make_tuple(10, 11), pm::with(std::make_tuple(11, 11), [] {
		return true;
	})));
}

void test_tuples_match_array()
{
	assert(pm::match(std::array{10, 11}
		| pm::with(std::make_tuple(10, 11), [] { return true; })));
	assert(!pm::match(std::array{10, 11}
		| pm::with(std::make_tuple(11, 11), [] { return true; })));

	// vice versa
	assert(pm::match(std::make_tuple(10, 11)
		| pm::with(std::array{10, 11}, [] { return true; })));
	assert(!pm::match(std::make_tuple(10, 11)
		| pm::with(std::array{11, 11}, [] { return true; })));
}

void test_tuples_match_pair()
{
	assert(pm::match(std::pair{10, 11}
		| pm::with(std::make_tuple(10, 11), [] { return true; })));
	assert(!pm::match(std::pair{10, 11}
		| pm::with(std::make_tuple(11, 11), [] { return true; })));

	// vice versa
	assert(pm::match(std::make_tuple(10, 11)
		| pm::with(std::pair{10, 11}, [] { return true; })));
	assert(!pm::match(std::make_tuple(10, 11)
		| pm::with(std::pair{11, 11}, [] { return true; })));
}

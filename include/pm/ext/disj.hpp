#ifndef PM_EXT_DISJ_HPP
#define PM_EXT_DISJ_HPP

#include <pm/ext/conj.hpp>

namespace pm {
namespace detail {

template <typename PatternRight>
struct open_disj : open_bin<PatternRight>
{
	using super_t = open_bin<PatternRight>;

	open_disj(PatternRight pr)
	: super_t(pr)
	{}
};

template <typename T>
concept is_open_disj = requires (T t) {
	{ pm::detail::open_disj{t} } -> std::same_as<T>;
};

template <typename PatternLeft, typename PatternRight>
struct disj : bin<PatternLeft, PatternRight>
{
	using super_t = bin<PatternLeft, PatternRight>;

	disj(PatternLeft pl, PatternRight pr)
	: super_t(pl, pr)
	{}
};

template <typename T>
concept is_disj = requires (T t) {
	{ pm::detail::disj{t} } -> std::same_as<T>;
};

/// Matcher for disj
template <typename Pattern, typename V>
	requires (detail::is_disj<Pattern>)
struct matcher<Pattern, V>
{
	bool matched = false;

	/// Matches if any pattern match value
	/// Matching is from left to right
	template <bool Strict=true>
	constexpr bool match(const Pattern &disj, auto &&v) {
		matched = (detail::get_matcher<decltype(disj.pattern_left), decltype(v)>()
			.match(disj.pattern_left, std::forward<decltype(v)>(v))
		|| detail::get_matcher<decltype(disj.pattern_right), decltype(v)>()
			.match(disj.pattern_right, std::forward<decltype(v)>(v)));
		return matched;
	}

	constexpr decltype(auto) result([[maybe_unused]] const Pattern &disj, auto &&v) noexcept {
		assert(matched);
		return std::forward<decltype(v)>(v);
	}
};

} // namespace detail

template <typename PatternLeft, typename PatternRight>
constexpr detail::is_disj auto disj(PatternLeft pl, PatternRight pr) noexcept
{
	return detail::disj(pl, pr);
}

template <typename PatternRight>
constexpr detail::is_open_disj auto disj(PatternRight pr) noexcept
{
	return detail::open_disj(pr);
}

} // namespace pm

/// Close open disj by joining it with another pattern into a complete disj
template <typename PatternLeft, typename Opendisj>
	requires (pm::detail::is_open_disj<Opendisj>)
constexpr pm::detail::is_disj auto operator+ (PatternLeft pl, Opendisj d) noexcept
{
	return pm::disj(pl, d.pattern_right);
}

#endif // PM_EXT_DISJ_HPP

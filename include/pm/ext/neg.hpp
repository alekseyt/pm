#ifndef PM_EXT_NEG_HPP
#define PM_EXT_NEG_HPP

namespace pm {
namespace detail {

struct open_neg {};

template <typename T>
concept is_open_neg = requires (T t) {
	{ pm::detail::open_neg{t} } -> std::same_as<T>;
};

template <typename Subpattern>
struct neg : open_neg
{
	using super_t = detail::open_neg;
	using subpattern_t = Subpattern;

	explicit constexpr neg(Subpattern p)
	: subpattern(p)
	{}

	subpattern_t subpattern;
};

template <typename T>
concept is_neg = requires (T t) {
	{ pm::detail::neg{t} } -> std::same_as<T>;
};

/// Matcher for neg
template <typename Pattern, typename V>
	requires (detail::is_neg<Pattern>)
struct matcher<Pattern, V>
{
	bool matched = false;

	/// Matches if subpattern does not match value
	template <bool Strict=true>
	constexpr bool match(const Pattern &neg, auto &&v) {
		matched = !(detail::get_matcher<decltype(neg.subpattern), decltype(v)>()
			.match(neg.subpattern, std::forward<decltype(v)>(v)));
		return matched;
	}

	constexpr decltype(auto) result([[maybe_unused]] const Pattern &neg, auto &&v) noexcept {
		assert(matched);
		return std::forward<decltype(v)>(v);
	}
};

} // namespace detail

template <typename Pattern>
constexpr detail::is_neg auto neg(Pattern p) noexcept
{
	return detail::neg(p);
}

constexpr detail::is_open_neg auto neg() noexcept
{
	return detail::open_neg();
}

} // namespace pm

/// Close open neg by joining it with (sub)pattern into a complete neg
template <typename Subpattern, typename Openneg>
	requires (pm::detail::is_open_neg<Openneg>)
constexpr pm::detail::is_neg auto operator+ (Subpattern p, [[maybe_unused]] Openneg n) noexcept
{
	return pm::neg(p);
}

#endif // PM_EXT_NEG_HPP

#ifndef PM_EXT_CONS_HPP
#define PM_EXT_CONS_HPP

#include <cassert>
#include <concepts>
#include <tuple>
#include <pm/detail/matcher.hpp>
#include <pm/ext/decons.hpp>

namespace pm {
namespace detail {

/// Open-expression cons. Requires subpattern to be closed.
template <typename Constructor>
struct open_cons
{
	using constructor_t = Constructor;

	constructor_t constructor;
};

template <typename T>
concept is_open_cons = requires (T t) {
	{ pm::detail::open_cons{t} } -> std::same_as<T>;
};

/// Constructing pattern
template <typename Pattern, typename Constructor>
struct cons : open_cons<Constructor>
{
	using explicit_variant_matching = std::true_type;

	using super_t = detail::open_cons<Constructor>;
	using pattern_t = Pattern;

	explicit constexpr cons(Pattern p, Constructor constructor)
	: super_t(constructor)
	, pattern(p)
	{}

	pattern_t pattern;
};

template <typename T>
concept is_cons = requires (T t) {
	{ pm::detail::cons{t} } -> std::same_as<T>;
};

/// cons matcher
template <typename Pattern, typename V>
	requires (detail::is_cons<std::remove_cvref_t<Pattern>>)
struct matcher<Pattern, V>
{
	using pattern_t = typename Pattern::pattern_t;
	using constructor_t = typename Pattern::constructor_t;
	using matcher_t = detail::matcher<pattern_t, V>;
	using match_result_t = decltype(std::declval<matcher_t>().result(std::declval<pattern_t>(), std::declval<V>()));

	bool matched = false;
	matcher_t matcher; // create matcher, so it can hold side effect created during matching

	template <bool Strict=true>
	constexpr bool match(const Pattern &cons, auto &&v) {
		matched = matcher.match(cons.pattern, v);
		return matched;
	}

	/// Constructor is invocable with match result
	constexpr decltype(auto) result(const Pattern &cons, auto &&v) /*const*/
		requires (std::invocable<constructor_t, match_result_t>)
	{
		assert(matched);
		return std::invoke(cons.constructor,
			matcher.result(cons.pattern, std::forward<decltype(v)>(v)));
	}

	/// If match result is decons - use std::apply to call constructor
	constexpr decltype(auto) result(const Pattern &cons, auto &&v) /*const*/
		requires (detail::is_decons<match_result_t>)
	{
		assert(matched);
		return std::apply(cons.constructor,
			*(matcher.result(cons.pattern, std::forward<decltype(v)>(v))));
	}
};

} // namespace detail

template <typename Subpattern, typename Constructor>
constexpr detail::is_cons auto cons(Subpattern p, Constructor constructor) noexcept
{
	return detail::cons(p, constructor);
}

/// Create open cons (without subpattern)
template <typename Constructor>
constexpr detail::is_open_cons auto cons(Constructor constructor) noexcept
{
	return detail::open_cons(constructor);
}

} // namespace pm

/// Close open cons by joining it with (sub)pattern into a complete cons
template <typename Subpattern, typename OpenCons>
	requires (pm::detail::is_open_cons<OpenCons>)
constexpr pm::detail::is_cons auto operator+ (Subpattern p, OpenCons c) noexcept
{
	return pm::cons(p, c.constructor);
}

/// Close open decons by cons with forwarding constructor
template <typename Subpattern, typename OpenDecons>
	requires (pm::detail::is_open_decons<OpenDecons>)
constexpr pm::detail::is_cons auto operator+ (Subpattern p, [[maybe_unused]] OpenDecons d) noexcept
{
	return pm::cons(p, [] (auto &&tuple_like) {
		return pm::decons(std::forward<decltype(tuple_like)>(tuple_like));
	});
}

#endif // PM_EXT_CONS_HPP

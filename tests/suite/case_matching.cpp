#include <cassert>
#include <pm/ext/any_of.hpp>
#include <pm/ext/within.hpp>
#include <pm/pm.hpp>

// 5.3.2.4 Case Pattern
// The case pattern has the form:
//   case expression-pattern
// And matches value v if expression-pattern matches v. This pattern allows using
// id-expression as part of inspect expression. Otherwise any identifier would have
// been interpreted as identifier pattern.

void test_case_enum_matching()
{
	enum class Color {
		red,
		green,
		blue,
	};

	const Color c = Color::green;
	assert(pm::match(c
		| pm::with(Color::green, [] { return true; })));
	assert(!pm::match(c
		| pm::with(Color::red, [] { return true; })));
	assert(!pm::match(c
		| pm::with(Color::blue, [] { return true; })));

	// composition with any_of
	assert(pm::match(c
		| pm::with(pm::any_of(Color::red, Color::green, Color::blue), [] { return true; })));
	// composition with within
	assert(pm::match(c
		| pm::with(pm::within(Color::red, Color::blue), [] { return true; })));
}

void test_case_mixed_value_const_matching()
{
	static constexpr int zero = 0;
	int v = 1;

	const auto match_1 = pm::match(v
		| pm::with(zero, [] { return 10; })
		| pm::with(1, [] { return 11; })
		| pm::with(2, [] { return 12; }));
	assert(match_1 && *match_1 == 11);

	// composition with any_of
	const auto match_2 = pm::match(v
		| pm::with(pm::any_of(zero, 1, 2), [] (int x) { return x * 3; }));
	assert(match_2 && *match_2 == 3);
}

void test_case_pair_matching()
{
	static constexpr int zero = 0, one = 1;
	std::pair<int, int> p = { 0, 1 };

	assert(pm::match(std::tie(p.first, p.second)
		| pm::with(std::tie(zero, one), [] { return true; })));
}

#ifndef PM_DETAIL_CONCEPTS_HPP
#define PM_DETAIL_CONCEPTS_HPP

#include <any>
#include <concepts>
#include <memory>
#include <optional>
#include <tuple>
#include <variant>

namespace pm {

// concepts related to standard types used	internally across pm
namespace detail {

// https://stackoverflow.com/a/54182690/796121

template <typename T>
concept is_variant = requires (T t) {
	{ std::variant{t} } -> std::same_as<T>;
};

template <typename T>
concept is_tuple = requires (T t) {
	{ std::tuple{t} } -> std::same_as<T>;
};

template <typename T>
concept is_any = requires (T t) {
	{ std::any{t} } -> std::same_as<T>;
};

template <typename T>
concept is_optional = requires (T t) {
	{ std::optional{t} } -> std::same_as<T>;
};

} // namespace detail

namespace detail {

template <typename T>
concept is_tuple_like = requires {
	std::tuple_size<T>{}; // check that std::tuple_size<T> can be instantiated
};

// if pattern satisfies this concept variant matcher won't try
// to implicitly match with this pattern like with any other value
// (that don't satisfy this concept)
// if pattern want to match with variant, it has to provide
// corresponding matcher explicitly or deconstruct itself into
// components that can match with variant and match components
template <typename T>
concept explicit_variant_matching = requires {
	typename T::explicit_variant_matching;
};

} // namespace detail

// pm-specific concepts for reporting errors earlier up the call stack
namespace detail {

template <typename MatcherType, typename Pattern, typename V, bool Strict>
concept can_match_pattern_with_v =
requires (MatcherType m, Pattern p, V &&v) {
	{ m.template match<Strict>(p, v) } -> std::same_as<bool>; // no matcher for matching pattern with v
};

template <typename DispatcherType, typename Callable, typename MatcherResultType>
concept matcher_result_fits_callback_args =
requires (DispatcherType d, Callable c, MatcherResultType &&r) {
	{ d.dispatch(c, r) }; // matcher result must fit match callback args
};

} // namespace detail
} // namespace pm

#endif // PM_DETAIL_CONCEPTS_HPP

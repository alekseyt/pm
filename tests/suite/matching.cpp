#include <cassert>
#include <string>
#include <pm/ext.hpp>
#include <pm/pm.hpp>

void test_compile_errors()
{
	// this should produce compile-time error
	// due to different return types
	// assert(pm::match(true
	// 	| pm::with(false, [] { return true; })
	// 	| pm::with(true, [] { return "abc"; })));

	// this should produce compile-time error
	// due to matcher's match can not be invoked
	// for string -> int in strict mode
	// assert(pm::match("abc"
	// 	| pm::with(10, [] { return true; })));
	// same as above for pass
	// pm::match("abc"
	// 	| pm::pass(10, [] { return true; }));

	// this should fail to compile because guard
	// is expecting non-const reference to matched value
	// int x = 10;
	// assert(pm::match(x
	// 	| pm::with(pm::__,
	// 		[] (int &) { return true; },
	// 		[] (int &) { return true; })));
}

void test_match_return()
{
	assert(pm::match(10
		| pm::with(10, [] { return true; })));
	assert(!pm::match(10
		| pm::with(11, [] { return true; })));

	// struct return
	struct S
	{
		S() = delete;
		explicit S(int s) : s(s) {}
		constexpr int operator* () const {
			return s;
		}

		const int s = 0;
	};

	auto x = pm::match(10
		| pm::with(10, [] { return S(10); }));
	assert(x && x->s == 10);

	assert(!pm::match(10
		| pm::with(11, [] { return S(11); })));
}

void test_match_return_void()
{
	bool callback_invoked = false;
	pm::match(10
		| pm::with(11, [&] { callback_invoked = true; })); // void return
	assert(!callback_invoked);

	pm::match(10
		| pm::with(10, [&] { callback_invoked = true; })); // void return
	assert(callback_invoked);
}

void test_match_chaining()
{
	const auto match_1 = pm::match(10
		| pm::with(10, [] { return 1; })
		| pm::with(11, [] { return 2; }));
	assert(match_1 && *match_1 == 1);

	const auto match_2 = pm::match(11
		| pm::with(10, [] { return 1;})
		| pm::with(11, [] { return 2; }));
	assert(match_2 && *match_2 == 2);

	const auto match_3 = pm::match(17
		| pm::with(10, [] { return 1; })
		| pm::with(11, [] { return 2; })
		| pm::with(7, [] { return 3; }));
	assert(!match_3);
}

// Capture value in callback
void test_match_capture_matched()
{
	const int x = 10;
	assert(pm::match(x
		| pm::with(pm::any_of(9, 10, 11), [&x] { return true; })));

	const std::variant<int, std::string> v = "abc";
	assert(pm::match(v
		| pm::with(pm::any_of(10, "abc"), [&v] {
			return std::visit([] (auto &&) {
				return true;
			}, v);
		})));

	struct S
	{
		int x = 10;
		std::string s = "abc";
	};

	const S s;
	assert(pm::match(std::tie(s.x, s.s)
		| pm::with(std::make_tuple(s.x, s.s), [x = s.x, s = s.s] { return true; })));
}

void test_match_pass()
{
	bool pass_called = false;
	const auto match = pm::match(2
		| pm::with(1, [] { return 1; }) // doesn't match this one
		| pm::pass(2, [&] { pass_called = true; }) // should skip this one, but invoke callback
		| pm::with(2, [] { return 3; })); // matches this one
	assert(match && *match == 3);
	assert(pass_called);
}

void test_match_double_pass()
{
	bool match_1_pass_called = false;
	const auto match_1 = pm::match(1
		| pm::pass(1, [&] { match_1_pass_called = true; return 1; }) // should skip this one
		| pm::with(1, [] { return 2; }));
	assert(match_1 && *match_1 == 2);
	assert(match_1_pass_called);

	bool match_2_pass_1_called = false, match_2_pass_2_called = false;
	const auto match_2 = pm::match(1
		| pm::pass(1, [&] { match_2_pass_1_called = true; return 1; }) // should skip this one
		| pm::pass(1, [&] { match_2_pass_2_called = true; return 2; }) // and skip this one
		| pm::with(1, [] { return 3; }));
	assert(match_2 && *match_2 == 3);
	assert(match_2_pass_1_called && match_2_pass_2_called);
}

void test_match_pass_always()
{
	bool pass_1_called = false, pass_2_called = false;
	pm::match(1
		| pm::pass(1, [&] { pass_1_called = true; return 1; }) // should skip this one
		| pm::pass(1, [&] { pass_2_called = true; return 2; })); // and skip this one too
	assert(pass_1_called && pass_2_called);
}

// Check that pass() invoked last in the chain handled correctly
void test_match_pass_last()
{
	bool pass_called_1 = false;
	const auto match_1 = pm::match(1
		| pm::with(1, [] { return 1; }) // should match this one
		| pm::pass(1, [&] { pass_called_1 = true; })); // shouldn't be called, change return type
	assert(match_1 && *match_1 == 1);
	assert(!pass_called_1);

	// same as above, but no match and piping whole expression
	bool pass_called_2 = false;
	const auto match_2 = pm::match(1
		| pm::with(2, [] { return 1; })
		| pm::pass(2, [&] { pass_called_2 = true; }));
	assert(!match_2);
	assert(!pass_called_2);
}

void test_match_pass_chaining()
{
	// extra: a bit of chaining of pass/nopass
	bool pass_1_called = false, pass_2_called = false;
	const auto match = pm::match(1
		| pm::with(2, [] { return 1; })
		| pm::pass(1, [&] { pass_1_called = true; })
		| pm::with(2, [] { return 3; })
		// this doesn't match, so callback isn't invoked
		| pm::pass(3, [&] { pass_2_called = true; })
		| pm::with(1, [] { return 5; }));
	assert(match && *match == 5);
	assert(pass_1_called);
	assert(!pass_2_called);
}

void test_match_pass_return_type()
{
	bool pass_called = false;
	const auto match = pm::match(1
		| pm::with(2, [] { return 1; })
		// should not make whole match expression return type into string
		// neither should conflict with with() return types
		| pm::pass(1, [&] { pass_called = true; return "abc"; })
		| pm::with(3, [] { return 3; })
		| pm::with(pm::__, [] { return 4; }));
	assert(match && *match == 4);
	assert(pass_called);
}

void test_match_pass_callback()
{
	bool pass_called = false;

	const auto match_call = [&] {
		// pass() maintains void return type, so `return x` does nothing
		return pm::match(1 | pm::pass(1, [&] (int x) { pass_called = true; return x; }));
	};

	static_assert(std::same_as<void, decltype(match_call())>);

	// call match, callback should be invoked anyway
	match_call();
	assert(pass_called);
}

void test_match_struct()
{
	struct S
	{
		int x = 0;
		std::string s = "abc";

		bool operator== (const S &s) const = default;
	}
	const s;

	static_assert(std::equality_comparable<S>);

	assert(pm::match(s
		| pm::with(s, [] { return true; })));
}

void test_match_arguments_basic()
{
	// callback with no arguments
	assert(pm::match(true
		| pm::with(true, [] { return true; })));
	// callback with argument
	assert(pm::match(false
		| pm::with(false, [] (bool b) { return !b; })));
	assert(pm::match(false
		| pm::pass(false, [] (bool) {})
		| pm::with(pm::__, [] { return true; })));
}

void test_match_arguments_tuples()
{
	// tuples
	const auto t = std::make_tuple(10, 11);
	const auto tuples_match = pm::match(t
		| pm::with(std::make_tuple(pm::__, 11), [] (std::tuple<int, int> t) { return std::get<1>(t); }));
	assert(tuples_match && *tuples_match == 11);
}

void test_match_arguments_variant()
{
	std::variant<int, std::string> v = 10;

	const auto match_1 = pm::match(v
		| pm::with(10, [] (int x) { return x * 2; }));
	assert(match_1 && *match_1 == 10 * 2);

	v = "11";
	const auto match_2 = pm::match(v
		| pm::with("11", [] (std::string x) { return x + x; }));
	assert(match_2 && *match_2 == "1111");

	v = 12;
	const auto match_3 = pm::match(v
		| pm::with(12, [] (const std::variant<int, std::string> &x) { return std::get<int>(x) * 2; }));
	assert(match_3 && *match_3 == 12 * 2);
}

void test_match_arguments_any_of()
{
	// any_of with ints
	const auto match_1 = pm::match(10
		| pm::with(pm::any_of(9, 10, 11), [] (int x) { return x; }));
	assert(match_1 && *match_1 == 10);

	// mixed any_of
	const auto match_2 = pm::match(10
		| pm::with(pm::any_of(10, "11"), [] (int x) { return x; }));
	assert(match_2 && *match_2 == 10);

	const auto match_3 = pm::match("11"
		| pm::with(pm::any_of(10, "11"), [] (std::string x) { return x; }));
	assert(match_3 && *match_3 == std::string("11"));
}

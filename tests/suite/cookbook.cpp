
// collection of examples from cookbook
// doesn't assert anything, just compile check
// real tests are supposed to be in relevant files

#include <cassert>
#include <ctime>
#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <variant>
#include <pm/ext.hpp>
#include <pm/pm.hpp>

using namespace pm;
using namespace std;

void test_cookbook_5_3_1()
{
	random_device rd;
	mt19937 gen(time(0));
	uniform_int_distribution<int> distrib(0, 5);

	const int x = distrib(gen);
	match(x
		| with(0,  [] { return "Zero"; })
		| with(1,  [] { return "One"; })
		| with(2,  [] { return "Two"; })
		| with(3,  [] { return "Three"; })
		| with(__, [] { return "Other"; }));
}

void test_cookbook_5_3_2_1()
{
	struct S {
		int x = 42;
		string s;
	} const s;

	match(tie(s.x, s.s)
		| with(make_tuple(42, __) + decons(),
			[] (int x, const string &y) { return to_string(x) + y; }));
}

void test_cookbook_5_3_2_2__1()
{
	variant<string, int> v = 10;

	match(v
		| with(pm::any_of(10, "abc"), [] (auto) {}));
}

void test_cookbook_5_3_2_2__3()
{
	// polymorphic_matching.cpp
	struct Shape { virtual ~Shape() = default; };
	struct Circle : Shape { float r = 7; };
	struct Rectangle : Shape { int w = 5; int h = 11; };

	unique_ptr<Shape> rectangle_ptr(new Rectangle());
	Shape *shape_ptr = rectangle_ptr.get();

	match(shape_ptr
		| with(pm::type<Circle>{}, [] (Circle &c) -> int {
			return 3.14 * c.r * c.r;
		})
		| with(pm::type<Rectangle>{}, [] (Rectangle &r) {
			return r.w * r.h;
		}));
}

void test_cookbook_5_3_2_3()
{
	struct Point { int x; int y; };
	struct Line { Point from; Point to; };

	std::variant<Point, Line> v = Point{0, 0};
	match(v
		| with(pm::type<Point>{}
			+ cons([] (auto &v) { return get<Point>(v); }),
			[] (const Point &p) { return p.x + p.y; }));
}

void test_cookbook_5_3_2_5()
{
	struct Node {
		int value;
		std::unique_ptr<Node> lhs, rhs;
	} node;

	match(tie(node.value, node.lhs)
		| with(make_tuple(__, nullptr) + decons(),
			[] (int /*value*/, std::unique_ptr<Node> &/*lhs*/) {})
		| with(make_tuple(__, maybe(__)) + decons(),
			[] (int /*value*/, Node &/*lhs*/) {})
	);
}

void test_cookbook_5_3_2_6()
{
	struct String {
		optional<string> try_extract(int x) {
			return make_optional(to_string(x));
		}
	};

	match(10
		| with(try_extract(String{}), [] (const string &) {}));

	struct Int {
		int extract(const string &s) {
			return atoi(s.c_str());
		}
	};

	match("10"
		| with(extract(Int{}), [] (int) {}));
}

void test_cookbook_5_4()
{
	match(10 | with(__,
		[] (int x) { return x <= 11; }, // guard
		[] { return true; })); // callback
}

// propagate side effect from match callback to matcher
void test_cookbook_side_effect_propagation()
{
	// propagation to matched value
	struct PimpMyString {
		std::string& extract(std::string &s) {
			return s;
		}
	};

	std::string s;
	match(s
		| with(extract(PimpMyString{}),
			[] (string &s) { s = "oof"; }));

	std::cout << s << std::endl;

	assert(s == "oof");

	// propagation to matcher
	struct HoldMyString {
		std::string extract(const std::string &s) {
			return s;
		}
	};

	s = *match(std::string{}
		| with(extract(HoldMyString{}),
			[] (string &s) {
				s = "pff";
				return s;
			}));

	std::cout << s << std::endl;

	assert(s == "pff");

	// minimal example
	match(s | with(__, [] (string &s) { s = "tootooroo"; }));

	std::cout << s << std::endl;

	assert(s == "tootooroo");
}

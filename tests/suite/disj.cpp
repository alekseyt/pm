#include <cassert>
#include <pm/ext.hpp>
#include <pm/pm.hpp>

static_assert(pm::detail::has_matcher<pm::detail::disj<int, int>, int>);
static_assert(pm::detail::has_matcher<pm::detail::disj<bool, bool>, bool>);
static_assert(pm::detail::has_matcher<pm::detail::disj<float, float>, float>);

void test_disj()
{
	assert(pm::match(10
		| pm::with(pm::disj(10, 10), [] { return true; })));
	assert(pm::match(10
		| pm::with(pm::disj(10, 11), [] { return true; })));
	assert(pm::match(10
		| pm::with(pm::disj(11, 10), [] { return true; })));
	assert(pm::match(11
		| pm::with(pm::disj(10, 11), [] { return true; })));
	assert(pm::match(11
		| pm::with(pm::disj(11, 10), [] { return true; })));
	assert(!pm::match(9
		| pm::with(pm::disj(10, 11), [] { return true; })));
	assert(!pm::match(12
		| pm::with(pm::disj(10, 11), [] { return true; })));
}

void test_open_disj()
{
	assert(pm::match(10
		| pm::with(10 + pm::disj(10), [] { return true; })));
	assert(pm::match(11
		| pm::with(10 + pm::disj(11), [] { return true; })));
	assert(pm::match(11
		| pm::with(11 + pm::disj(10), [] { return true; })));
	assert(!pm::match(12
		| pm::with(10 + pm::disj(11), [] { return true; })));
}

// Test disjunction disjunction
void test_disj_disj()
{
	assert(pm::match(10
		| pm::with(10 + pm::disj(10), [] { return true; })));
	assert(pm::match(10
		| pm::with(10 + pm::disj(10) + pm::disj(10), [] { return true; })));
	assert(pm::match(10
		| pm::with(10 + pm::disj(11) + pm::disj(12), [] { return true; })));
	assert(pm::match(11
		| pm::with(10 + pm::disj(11) + pm::disj(12), [] { return true; })));
	assert(pm::match(12
		| pm::with(10 + pm::disj(11) + pm::disj(12), [] { return true; })));
	assert(!pm::match(9
		| pm::with(10 + pm::disj(11) + pm::disj(12), [] { return true; })));
}

#include <cassert>
#include <string>
#include <pm/pm.hpp>
#include "match.hpp"

static_assert(pm::detail::has_matcher<pm::Type<int>, pm::Type<int>>);
static_assert(pm::detail::has_matcher<pm::Type<int>, pm::Type<int>&>);
static_assert(pm::detail::has_matcher<pm::Type<int>, const pm::Type<int>&>);
static_assert(pm::detail::has_matcher<pm::Type<int>, pm::Type<int>&&>);

// Low-level matching
void test_types_to_types()
{
	assert(pm::detail::match(pm::type<std::string>{}, pm::type<std::string>{}));
	assert(!pm::detail::match(pm::type<std::string>{}, pm::type<int>{}));

	// mixing (not allowed)
	assert(!pm::detail::match(pm::type<std::string>{}, pm::type<const char *>{}));
	assert(!pm::detail::match(pm::type<const char *>{}, pm::type<std::string>{}));
	assert(!pm::detail::match(pm::type<int>{}, pm::type<float>{}));
	assert(!pm::detail::match(pm::type<float>{}, pm::type<double>{}));
	assert(!pm::detail::match(pm::type<double>{}, pm::type<int>{}));
}

// Low-level matching
void test_types_to_values()
{
	assert(pm::detail::match(pm::type(10), pm::type(int{})));
	assert(pm::detail::match(pm::type(10.0), pm::type(double{})));
	assert(pm::detail::match(pm::type(10.0f), pm::type(float{})));
	assert(!pm::detail::match(pm::type("abc"), pm::type(int{})));
}

// High-level matching
void test_types_match()
{
	assert(pm::match(pm::type(int{}), pm::with(pm::type(int{}), [] {
		return true;
	})));
	assert(!pm::match(pm::type(std::string{}), pm::with(pm::type(int{}), [] {
		return true;
	})));
}

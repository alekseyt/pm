#ifndef PM_EXT_ANY_HPP
#define PM_EXT_ANY_HPP

#include <cassert>
#include <concepts>
#include <pm/detail/concepts.hpp>
#include <pm/detail/matcher.hpp>

namespace pm {
namespace detail {

struct any
{
	using explicit_variant_matching = std::true_type;
};

template <typename T>
concept is_pm_any = requires (T t) {
	{ pm::detail::any{t} } -> std::same_as<T>;
};

template <typename Pattern, typename V>
	requires (detail::is_pm_any<Pattern>)
struct any_matcher
{
	/// Always returns true
	template <bool Strict=true>
	constexpr bool match([[maybe_unused]] auto &&p, [[maybe_unused]] auto &&v) noexcept {
		return true;
	}

	constexpr decltype(auto) result([[maybe_unused]] auto &&p, auto &&v) const {
		return std::forward<decltype(v)>(v);
	}
};

/// any -> value matcher
template <typename Pattern, typename V>
	requires (detail::is_pm_any<Pattern>)
struct matcher<Pattern, V> : any_matcher<Pattern, V> {};

} // namespace detail

/// Alias for pm::any
using any = detail::any;

/// Wildcard: pm:__
inline constexpr detail::any __;

} // namespace pm

#endif // PM_EXT_ANY_HPP

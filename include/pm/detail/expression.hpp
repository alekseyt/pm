#ifndef PM_DETAIL_EXPRESSION_HPP
#define PM_DETAIL_EXPRESSION_HPP

#include <pm/detail/adaptor.hpp>

namespace pm { namespace detail {

// Value to match + adaptor of conditions
template <typename V, typename Adaptor>
struct Expression
{
	using value_t = V;
	using adaptor_t = Adaptor;

	V value;
	Adaptor adaptor;
};

template <typename T>
concept is_expression = requires (T t) {
	typename T::value_t;
	typename T::adaptor_t;
	t.value;
	t.adaptor;
	detail::is_adaptor<decltype(t.adaptor)>;
};

/// Expression factory
template <typename V, typename Adaptor>
	requires (detail::is_adaptor<Adaptor>)
constexpr detail::is_expression auto make_expression(V &&v, Adaptor adaptor) noexcept
{
	return detail::Expression<V, Adaptor>{
		.value = std::forward<V>(v),
		.adaptor = std::forward<Adaptor>(adaptor)
	};
}

}} // namespace detail // namespace pm

#endif // PM_DETAIL_EXPRESSION_HPP

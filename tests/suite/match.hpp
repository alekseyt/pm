#ifndef TESTS_MATCH_HPP
#define TESTS_MATCH_HPP

#include <pm/detail/matcher.hpp>

namespace pm { namespace detail {

// For tests convenience, should not be used otherwise
constexpr bool match(auto &&v, auto &&pattern)
{
	return detail::get_matcher(pattern, v).match(pattern, v);
}

}} // namespace detail // namespace pm

#endif // TESTS_MATCH_HPP

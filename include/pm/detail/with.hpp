#ifndef PM_DETAIL_WITH_HPP
#define PM_DETAIL_WITH_HPP

#include <concepts>
#include <optional>
#include <pm/detail/clause.hpp>
#include <pm/detail/concepts.hpp>
#include <pm/detail/matcher.hpp>

namespace pm { namespace detail {

template <auto PatternGuard, typename GuardDispatcherType, typename MatcherResultType>
concept matcher_result_fits_guard_args =
requires (MatcherResultType r) {
	{ GuardDispatcherType{}.const_dispatch(PatternGuard, r) } -> std::same_as<bool>; // matcher result must fit pattern guard args
};

/// Used as a placeholder pattern guard, doesn't do anything,
/// it's only useful feature is to being able to match whatever match result
inline constexpr auto always_true = [] ([[maybe_unused]] auto &&...args) constexpr noexcept
{
	return true;
};

/// with clause
template <typename Pattern, typename Callable, auto PatternGuard=always_true>
struct with : detail::Clause<Pattern, Callable, false>
{
	using super_t = detail::Clause<Pattern, Callable, false>;

	explicit constexpr with(Pattern pattern, Callable c)
	: super_t(pattern, c)
	{}

	/// If pattern matches - invoke callback with match result
	template <bool Strict=true, typename V,
		typename MatcherType=decltype(detail::get_matcher<Pattern, V>()),
		typename MatcherResultType=decltype(std::declval<MatcherType>().result(
			std::declval<const Pattern>(), std::declval<V>())),
		typename DispatcherType=decltype(detail::get_dispatcher<Pattern, Callable, MatcherResultType>()),
		typename GuardDispatcherType=decltype(detail::get_dispatcher<Pattern, decltype(PatternGuard), MatcherResultType>())>
		requires (detail::can_match_pattern_with_v<MatcherType, Pattern, V, Strict>
		&& detail::matcher_result_fits_guard_args<PatternGuard, GuardDispatcherType, MatcherResultType>
		&& detail::matcher_result_fits_callback_args<DispatcherType, Callable, MatcherResultType>)
	constexpr detail::is_eval_result auto evaluate(V &&v) const
	{
		MatcherType matcher;

		const bool matched = matcher.template match<Strict>(this->pattern, v)
			&& GuardDispatcherType{}.const_dispatch(PatternGuard, matcher.result(this->pattern, v));

		DispatcherType dispatcher;

		const auto dispatch_callback = [&] () constexpr {
			return dispatcher.dispatch(this->callback, matcher.result(this->pattern, v));
		};

		using dispatch_result_t = decltype(dispatch_callback());

		// if callback returns void just invoke callback without recording result
		if constexpr (std::same_as<void, dispatch_result_t>) {
			if (matched) {
				dispatch_callback();
			}

			return detail::EvalResultVoid{ matched };
		}
		// otherwise return std::optional<V> where V is what callback returned
		else {
			return detail::EvalResultValue<dispatch_result_t>{ matched, (matched
				? std::make_optional(dispatch_callback())
				: std::optional<dispatch_result_t>(std::nullopt)) };
		}
	}
};

}} // namespace detail // namespace pm

#endif // PM_DETAIL_WITH_HPP

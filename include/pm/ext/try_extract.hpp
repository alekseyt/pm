#ifndef PM_EXT_TRY_EXTRACT_HPP
#define PM_EXT_TRY_EXTRACT_HPP

namespace pm {
namespace detail {

// 5.3.2.6 Extractor Pattern
// The extractor pattern has the following two forms:
//     ( constant-expression ! ) pattern
// --> ( constant-expression ? ) pattern
//
// For second form, let e be the result of a call to member c.try_extract(v)
// or else a non-member ADL-only try_extract(c, v). It matches value v if e
// is contextually convertible to bool, evaluates to true, and pattern matches *e.

template <typename Extractor, typename Subpattern>
struct try_extract : extract_base<Extractor, Subpattern>
{
	using super_t = extract_base<Extractor, Subpattern>;

	explicit constexpr try_extract(Extractor e, Subpattern p)
	: super_t(e, p)
	{}
};

template <typename T>
concept is_try_extract = requires (T t) {
	{ pm::detail::try_extract{t} } -> std::same_as<T>;
};

template <typename T>
concept is_conditional_extract = detail::is_try_extract<T>
	|| (detail::is_tentative_extract<T> && detail::is_try_extract<typename T::super_t>);

} // namespace detail

namespace try_extract_bp {

template <typename Extractor, typename V>
concept extractor_with_member = requires (Extractor e, V v) {
	{ e.try_extract(v) } -> detail::is_optional; // member version
};

template <typename Extractor, typename V>
concept extractor_with_adl = requires (Extractor e, V v) {
	{ try_extract(e, v) } -> detail::is_optional; // adl version
};

template <typename T>
	requires (std::default_initializable<T>)
struct extractor
{
	T extractor;

	constexpr auto perform_extract(auto &&v)
		requires (try_extract_bp::extractor_with_member<T, decltype(v)>)
	{
		return this->extractor.try_extract(std::forward<decltype(v)>(v));
	}

	constexpr auto perform_extract(auto &&v)
		requires (try_extract_bp::extractor_with_adl<T, decltype(v)>)
	{
		return try_extract(this->extractor, std::forward<decltype(v)>(v));
	}
};

} // try_extract_bp

namespace detail {

/// Matcher for exract with conditional extractor
template <typename Pattern, typename V>
	requires (detail::is_conditional_extract<Pattern>)
struct matcher<Pattern, V>
{
	using extractor_t = try_extract_bp::extractor<typename Pattern::extractor_t>;
	using extractor_result_t = decltype(std::declval<extractor_t>().perform_extract(std::declval<V>()));
	using subpattern_t = typename Pattern::subpattern_t;

	extractor_result_t extracted; // optional for conditional extractor

	/// Matches if value can be extracted and subpattern matches that
	template <bool Strict=true>
	constexpr bool match(const Pattern &extract, auto &&v) {
		extracted = extractor_t().perform_extract(v);
		if (!extracted) {
			return false;
		}

		if (detail::get_matcher(extract.subpattern, *extracted).match(extract.subpattern, *extracted)) {
			return true;
		}

		extracted.reset(); // reset, otherwise assert() in result() won't trigger
		return false;
	}

	constexpr decltype(auto) result([[maybe_unused]] auto &&p, [[maybe_unused]] auto &&v) const noexcept {
		assert(!!extracted);
		return *extracted;
	}
};

} // namespace detail

template <typename Extractor, typename Subpattern>
constexpr detail::is_try_extract auto try_extract(Extractor e, Subpattern p) noexcept
{
	return detail::try_extract(e, p);
}

template <typename Extractor>
constexpr detail::is_tentative_extract auto try_extract(Extractor e) noexcept
{
	return detail::tentative_extract(detail::try_extract(e, pm::any()));
}

} // namespace pm

template <typename TentativeExtract, typename Subpattern>
	requires (pm::detail::is_tentative_extract<TentativeExtract>
	&& pm::detail::is_try_extract<typename TentativeExtract::super_t>)
constexpr pm::detail::is_try_extract auto operator% (TentativeExtract te, Subpattern p) noexcept
{
	return pm::detail::try_extract(te.extractor, p);
}

#endif // PM_EXT_TRY_EXTRACT_HPP

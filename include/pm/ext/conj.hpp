#ifndef PM_EXT_CONJ_HPP
#define PM_EXT_CONJ_HPP

namespace pm {
namespace detail {

/// Open binary pattern holds *right* pattern
/// This way pattern + open_bin(pattern) keeps the left-to-right semantics
template <typename PatternRight>
struct open_bin
{
	using pattern_right_t = PatternRight;

	pattern_right_t pattern_right;
};

template <typename T>
concept is_open_bin = requires (T t) {
	{ pm::detail::open_bin{t} } -> std::same_as<T>;
};

template <typename PatternLeft, typename PatternRight>
struct bin : open_bin<PatternRight>
{
	using super_t = detail::open_bin<PatternRight>;
	using pattern_left_t = PatternLeft;

	explicit constexpr bin(PatternLeft pl, PatternRight pr)
	: super_t(pr)
	, pattern_left(pl)
	{}

	pattern_left_t pattern_left;
};

template <typename T>
concept is_bin = requires (T t) {
	{ pm::detail::bin{t} } -> std::same_as<T>;
};

template <typename PatternRight>
struct open_conj : open_bin<PatternRight>
{
	using super_t = open_bin<PatternRight>;

	open_conj(PatternRight pr)
	: super_t(pr)
	{}
};

template <typename T>
concept is_open_conj = requires (T t) {
	{ pm::detail::open_conj{t} } -> std::same_as<T>;
};

template <typename PatternLeft, typename PatternRight>
struct conj : bin<PatternLeft, PatternRight>
{
	using super_t = bin<PatternLeft, PatternRight>;

	conj(PatternLeft pl, PatternRight pr)
	: super_t(pl, pr)
	{}
};

template <typename T>
concept is_conj = requires (T t) {
	{ pm::detail::conj{t} } -> std::same_as<T>;
};

/// Matcher for conj
template <typename Pattern, typename V>
	requires (detail::is_conj<Pattern>)
struct matcher<Pattern, V>
{
	bool matched = false;

	/// Matches if both patterns match value
	/// Matching is from left to right
	template <bool Strict=true>
	constexpr bool match(const Pattern &conj, auto &&v) {
		matched = (detail::get_matcher<decltype(conj.pattern_left), decltype(v)>()
			.match(conj.pattern_left, std::forward<decltype(v)>(v))
		&& detail::get_matcher<decltype(conj.pattern_right), decltype(v)>()
			.match(conj.pattern_right, std::forward<decltype(v)>(v)));
		return matched;
	}

	constexpr decltype(auto) result([[maybe_unused]] const Pattern &conj, auto &&v) noexcept {
		assert(matched);
		return std::forward<decltype(v)>(v);
	}
};

} // namespace detail

template <typename PatternLeft, typename PatternRight>
constexpr detail::is_conj auto conj(PatternLeft pl, PatternRight pr) noexcept
{
	return detail::conj(pl, pr);
}

template <typename PatternRight>
constexpr detail::is_open_conj auto conj(PatternRight pr) noexcept
{
	return detail::open_conj(pr);
}

} // namespace pm

/// Close open conj by joining it with another pattern into a complete conj
template <typename PatternLeft, typename Openconj>
	requires (pm::detail::is_open_conj<Openconj>)
constexpr pm::detail::is_conj auto operator+ (PatternLeft pl, Openconj c) noexcept
{
	return pm::conj(pl, c.pattern_right);
}

#endif // PM_EXT_CONJ_HPP

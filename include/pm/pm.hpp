#ifndef PM_PM_HPP
#define PM_PM_HPP

#include <pm/adaptor.hpp>
#include <pm/match.hpp> // FIXME: order matters
#include <pm/pass.hpp>
#include <pm/type.hpp>
#include <pm/with.hpp>

#endif // PM_PM_HPP

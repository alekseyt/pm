
// compile this first

#include <pm/ext.hpp>
#include <pm/pm.hpp>

// detail::is_variant
static_assert(pm::detail::is_variant<std::variant<int>>);
static_assert(pm::detail::is_variant<std::variant<int, float>>);

// detail::is_tuple
static_assert(pm::detail::is_tuple<std::tuple<int>>);
static_assert(pm::detail::is_tuple<std::tuple<int, int>>);

// detail::is_any
static_assert(pm::detail::is_any<std::any>);

// detail::is_optional
static_assert(pm::detail::is_optional<std::optional<int>>);

// detail::is_tuple_like
static_assert(pm::detail::is_tuple_like<std::tuple<int>>);
static_assert(pm::detail::is_tuple_like<std::array<int, 1>>);
static_assert(pm::detail::is_tuple_like<std::pair<int, int>>);
// variant should be excluded from is_tuple_like
static_assert(!pm::detail::is_tuple_like<std::variant<int, float>>);

// pointers
static_assert(pm::detail::is_pointer<int *>);
static_assert(pm::detail::is_pointer<const int *>);
static_assert(pm::detail::is_pointer<std::nullptr_t>);
static_assert(!pm::detail::is_pointer<int>);
static_assert(!pm::detail::is_pointer<std::tuple<int>>);

// smart pointers
static_assert(pm::detail::is_smart_pointer<std::unique_ptr<int>>);
static_assert(pm::detail::is_smart_pointer<std::shared_ptr<int>>);
static_assert(!pm::detail::is_smart_pointer<int>);
static_assert(!pm::detail::is_smart_pointer<std::tuple<int>>);

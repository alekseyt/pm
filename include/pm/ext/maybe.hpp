#ifndef PM_EXT_MAYBE_HPP
#define PM_EXT_MAYBE_HPP

#include <cassert>
#include <concepts>
#include <utility>
#include <pm/detail/concepts.hpp>
#include <pm/detail/matcher.hpp>

namespace pm {
namespace detail {

/// 5.3.2.5 Dereference Pattern
/// The dereference pattern has the following forms:
///     (*!) pattern
/// --> (*?) pattern
/// The second form matches value v if v is contextually
/// convertible to bool and evaluates to true, and pattern matches *v.
template <typename Subpattern>
struct maybe
{
	using subpattern_t = Subpattern;

	explicit constexpr maybe(Subpattern p)
	: subpattern(p)
	{}

	subpattern_t subpattern;
};

template <typename T>
concept is_maybe = requires (T t) {
	{ pm::detail::maybe{t} } -> std::same_as<T>;
};

/// Base class for maybe and surely
template <typename Pattern, typename V>
	requires (requires (Pattern p, V v) {
		typename Pattern::subpattern_t; // pattern must define subpattern_t
		{ *v }; // value must be dereferencible
	})
struct dereferencing_matcher_base
{
	using pattern_t = typename Pattern::subpattern_t;
	using dereferenced_value_t = std::remove_reference_t<decltype(*std::declval<V>())>;
	using matcher_t = detail::matcher<pattern_t, dereferenced_value_t>;
};

/// maybe matcher
template <typename Pattern, typename V>
	requires (detail::is_maybe<std::remove_cvref_t<Pattern>>)
struct matcher<Pattern, V>
{
	using super_t = dereferencing_matcher_base<Pattern, V>;

	typename super_t::matcher_t matcher; // matcher to hold potential side effect

	// True if value converted to bool is true and dereferenced value matches arg
	template <bool Strict=true>
		// value must be implicitly convertible to bool and dereferencible
		requires (requires(V v) {
			{ !!v } -> std::same_as<bool>;
			{ *v };
		})
	constexpr bool match(const Pattern &maybe, auto &&v) {
		return (!!v && this->matcher.match(maybe.subpattern, *v));
	}

	/// Returns match result of subpattern with dereferenced value
	constexpr decltype(auto) result(const Pattern &maybe, auto &&v) const {
		return matcher.result(maybe.subpattern, *v);
	}
};

} // namespace detail

template <typename Pattern>
constexpr detail::is_maybe auto maybe(Pattern p) noexcept
{
	return detail::maybe(p);
}

} // namespace pm

#endif // PM_EXT_MAYBE_HPP

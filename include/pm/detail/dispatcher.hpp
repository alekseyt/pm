#ifndef PM_DETAIL_DISPATCHER_HPP
#define PM_DETAIL_DISPATCHER_HPP

#include <concepts>
#include <functional>

namespace pm { namespace detail {

/// Struct for dispatching value to callback.
template <typename Pattern, typename Callable, typename V>
struct dispatcher
{
	/// Overload used to pass value to callback as-is if it accepts it
	decltype(auto) dispatch(const Callable &c, auto &&v)
		requires (std::invocable<Callable, decltype(v)>
		// remove ambiguity for detail::always_true that can be called
		// with or without any arguments
		&& !std::invocable<Callable>)
	{
		return std::invoke(c, std::forward<decltype(v)>(v));
	}

	/// Overload used when callback doesn't take arguments
	decltype(auto) dispatch(const Callable &c, [[maybe_unused]] auto &&v)
		requires (std::invocable<Callable>)
	{
		return std::invoke(c);
	}

	decltype(auto) const_dispatch(const Callable &c, const auto &v)
		requires (std::invocable<Callable, std::add_const_t<decltype(v)>>
		&& !std::invocable<Callable>)
	{
		return std::invoke(c, v);
	}

	decltype(auto) const_dispatch(const Callable &c, [[maybe_unused]] const auto &v)
		requires (std::invocable<Callable>)
	{
		return dispatch(c, v);
	}
};

template <typename Pattern, typename Callable, typename V>
constexpr auto get_dispatcher() noexcept
{
	return detail::dispatcher<std::decay_t<Pattern>, Callable, V>{};
}

// no get_dispatcher() by values because this would require
// to call matcher.result() which is generally unwanted

}} // namespace detail // namespace pm

#endif // PM_DETAIL_DISPATCHER_HPP

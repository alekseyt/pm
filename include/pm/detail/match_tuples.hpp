#ifndef PM_DETAIL_MATCH_TUPLES_HPP
#define PM_DETAIL_MATCH_TUPLES_HPP

#include <cassert>
#include <concepts>
#include <pm/detail/concepts.hpp>
#include <pm/detail/matcher.hpp>

namespace pm { namespace detail {

/// Every element of value tuple is matched to every element of arg tuple
/// Depends on detail::match<tuple, tuple>() when tuples are nested
template <std::size_t I, typename Matchers, typename Patterns, typename Values>
	requires (I < std::tuple_size_v<Patterns>
	&& std::tuple_size_v<Patterns> == std::tuple_size_v<std::remove_cvref_t<Values>>)
constexpr bool match_tuples(Matchers &matchers, const Patterns &p, const Values &v)
{
	auto &matcher = std::get<I>(matchers);
	if (!matcher.match(std::get<I>(p), std::get<I>(v))) {
		return false;
	}

	if constexpr (I+1 < std::tuple_size_v<Patterns>) {
		return match_tuples<I+1>(matchers, p, v);
	}

	return true;
}

/// Create tuple of matchers that would match tuple of values
template <std::size_t I, typename Patterns, typename Values>
	requires (I < std::tuple_size_v<Patterns>
	&& std::tuple_size_v<Patterns> == std::tuple_size_v<std::remove_cvref_t<Values>>)
constexpr detail::is_tuple auto make_matchers() noexcept
{
	using pattern_t = std::tuple_element_t<I, Patterns>;
	using value_t = std::tuple_element_t<I, std::remove_cvref_t<Values>>;
	using matcher_t = decltype(detail::get_matcher(std::declval<pattern_t>(), std::declval<value_t>()));

	if constexpr (I+1 < std::tuple_size_v<Patterns>) {
		return std::tuple_cat(std::make_tuple(matcher_t{}), make_matchers<I+1, Patterns, Values>());
	}
	else {
		return std::make_tuple(matcher_t{});
	}
}

/// Create tuple of match results from tuple of matchers
template <std::size_t I, typename Matchers, typename Patterns, typename Values>
constexpr detail::is_tuple auto make_match_result(const Matchers &matchers, const Patterns &p, Values &&v)
{
	const auto &matcher = std::get<I>(matchers);

	if constexpr (I+1 < std::tuple_size_v<Matchers>) {
		return std::tuple_cat(std::forward_as_tuple(matcher.result(std::get<I>(p), std::get<I>(v))),
			make_match_result<I+1>(matchers, p, v));
	}
	else {
		return std::forward_as_tuple(matcher.result(std::get<I>(p), std::get<I>(v)));
	}
}

/// Matcher for tuples
/// Overall scheme:
/// 1. Make matchers for matching
/// 2. Match tuple with those matchers
/// 3. Make tuple of results from matchers
template <typename Pattern, typename V>
	requires (detail::is_tuple_like<std::remove_cvref_t<Pattern>>
	&& detail::is_tuple_like<std::remove_cvref_t<V>>
	&& (std::tuple_size_v<std::remove_cvref_t<V>> == std::tuple_size_v<std::remove_cvref_t<Pattern>>))
struct matcher<Pattern, V>
{
	// create matchers to hold side effect(s) created during matching
	decltype(make_matchers<0, Pattern, V>()) matchers;
	bool matched = false;

	/// Tuples match if all their elements match (logical-and)
	template <bool Strict=true>
	constexpr bool match(const Pattern &p, auto &&v) {
		matched = detail::match_tuples<0>(matchers, p, v);
		return matched;
	}

	constexpr auto result(const Pattern &p, auto &&v) const {
		assert(matched);
		return make_match_result<0>(this->matchers, p, v);
	}
};

}} // namespace detail // namespace pm

#endif // PM_DETAIL_MATCH_TUPLES_HPP

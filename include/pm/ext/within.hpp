#ifndef PM_EXT_WITHIN_HPP
#define PM_EXT_WITHIN_HPP

#include <cassert>
#include <concepts>
#include <utility>
#include <pm/detail/matcher.hpp>

namespace pm {
namespace detail {

template <typename T, typename U>
	requires (requires (T a, U b) {
		{ a <= b };
	})
struct within
{
	explicit constexpr within(const T &first, const U &last)
	: first(first)
	, last(last)
	{}

	T first;
	U last;
};

template <typename T>
concept is_within = requires (T t) {
	{ pm::detail::within{t} } -> std::same_as<T>;
};

/// within matcher
/// First and last might be swapped
/// First might be equal to last
template <typename Pattern, typename V>
	requires (detail::is_within<Pattern>
	&& requires (V v, Pattern w) {
		{ v >= w.first };
		{ v <= w.last };
	})
struct matcher<Pattern, V>
{
	bool matched = false;

	template <bool Strict=true>
	constexpr bool match(const Pattern &within, auto &&v) {
		matched = (within.first <= within.last
			? (v >= within.first && v <= within.last)
			: (v >= within.last && v <= within.first));

		return matched;
	}

	constexpr decltype(auto) result([[maybe_unused]] const Pattern &p, auto &&v) const {
		assert(matched);
		return std::forward<decltype(v)>(v);
	}
};

} // namespace detail

template <typename T, typename U>
constexpr detail::is_within auto within(const T &from, const U &to) noexcept
{
	return detail::within(from, to);
}

template <typename T>
	requires (requires (T t) {
		{ std::pair{t} } -> std::same_as<T>;
	})
constexpr detail::is_within auto within(const T &pair) noexcept
{
	return detail::within(pair.first, pair.second);
}

} // namespace pm

#endif // PM_EXT_WITHIN_HPP

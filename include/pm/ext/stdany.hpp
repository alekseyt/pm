#ifndef PM_EXT_STDANY_HPP
#define PM_EXT_STDANY_HPP

#include <any>
#include <pm/detail/concepts.hpp>
#include <pm/detail/matcher.hpp>
#include <pm/type.hpp>

namespace pm { namespace detail {

// 5.3.2.2 Alternative Pattern
// Case 2: std::any-like
//   < type > pattern
// If Alt is a type and there exists a valid non-member ADL-only
// any_cast<Alt>(&v), let p be its result. The alternative pattern matches if
// p contextually converted to bool evaluates to true, and pattern matches *p.

/// Matcher for type -> std::any
template <typename Pattern, typename V>
	requires (detail::is_type<std::remove_cvref_t<Pattern>>
	&& detail::is_any<std::remove_cvref_t<V>>)
struct matcher<Pattern, V>
{
	using pattern_type_t = typename Pattern::type_t;
	const pattern_type_t *matched = nullptr;

	/// True if stored value type matches value (which is type)
	template <bool Strict=true>
	constexpr bool match([[maybe_unused]] const Pattern &p, const V &any) noexcept {
		matched = std::any_cast<pattern_type_t>(&any);
		return (matched != nullptr);
	}

	constexpr decltype(auto) result([[maybe_unused]] const Pattern &p, [[maybe_unused]] auto &&v) const noexcept {
		assert(matched != nullptr);
		return *matched;
	}
};

}} // namespace detail // namespace pm

#endif // PM_EXT_STDANY_HPP

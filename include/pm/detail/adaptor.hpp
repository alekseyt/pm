#ifndef PM_DETAIL_ADAPTOR_HPP
#define PM_DETAIL_ADAPTOR_HPP

#include <tuple>
#include <pm/detail/concepts.hpp>

namespace pm { namespace detail {

/// Struct to collect all arguments before calling match()
template <typename Clauses>
	requires (detail::is_tuple<Clauses>
	&& std::tuple_size_v<Clauses> > 0)
struct Adaptor
{
	using clauses_t = Clauses;

	Clauses clauses;
};

template <typename T>
concept is_adaptor = requires (T t) {
	typename T::clauses_t;
	t.clauses;
	detail::is_tuple<typename T::clauses_t>;
	std::tuple_size_v<typename T::clauses_t> > 0;
};

/// Adaptors factory
template <typename Clauses>
// XXX: detail::is_clause can't be checked here because value type is yet unknown
constexpr detail::is_adaptor auto make_adaptor(Clauses clauses) noexcept
{
	return detail::Adaptor<Clauses>{
		.clauses = std::forward<Clauses>(clauses)
	};
}

}} // namespace detail // namespace pm

#endif // PM_DETAIL_ADAPTOR_HPP

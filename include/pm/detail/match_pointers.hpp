#ifndef PM_DETAIL_MATCH_POINTERS_HPP
#define PM_DETAIL_MATCH_POINTERS_HPP

#include <cassert>
#include <concepts>
#include <type_traits>
#include <pm/detail/matcher.hpp>
#include <pm/type.hpp>

namespace pm { namespace detail {

template <typename T>
concept is_pointer = std::is_pointer_v<T>
	|| std::same_as<std::nullptr_t, T>;

// smart pointer must have method `get()` that returns
// pointer to `element_type` defined in it
template <typename T>
concept is_smart_pointer = requires (T t) {
    typename T::element_type;
    { t.get() } -> std::same_as<typename T::element_type *>;
};

/// Matcher for two pointers (includes std::nullptr_t)
template <typename Pattern, typename V>
	requires (detail::is_pointer<std::remove_cvref_t<Pattern>>
	&& detail::is_pointer<std::remove_cvref_t<V>>)
struct matcher<Pattern, V>
{
	bool matched = false;

	/// Compare pointers using operator==
	template <bool Strict=true>
		requires (std::equality_comparable_with<Pattern, V>)
	constexpr bool match(Pattern p, V v) noexcept { // pass pointers by value
		matched = (p == v);
		return matched;
	}

	constexpr auto result([[maybe_unused]] auto p, auto v) const {
		assert(matched);
		return v;
	}
};

/// Match pointer (including nullptr) with smart pointer
template <typename Pattern, typename V>
	requires (detail::is_pointer<std::remove_cvref_t<Pattern>>
	&& detail::is_smart_pointer<std::remove_cvref_t<V>>)
struct smart_pointer_matcher
{
	bool matched = false;

	template <bool Strict=true>
	constexpr bool match(Pattern p, auto &&v) noexcept {
		matched = detail::get_matcher(p, v.get()).match(p, v.get());
		return matched;
	}

	constexpr decltype(auto) result([[maybe_unused]] auto p, auto &&v) const {
		assert(matched);
		return v;
	}
};

/// Matcher for pointers (match smart pointer)
template <typename Pattern, typename V>
	requires (detail::is_pointer<std::remove_cvref_t<Pattern>>
	&& detail::is_smart_pointer<std::remove_cvref_t<V>>)
struct matcher<Pattern, V> : smart_pointer_matcher<Pattern, V> {};

/// Matcher for smart pointers (match pointer)
template <typename Pattern, typename V>
	requires (detail::is_smart_pointer<std::remove_cvref_t<Pattern>>
	&& detail::is_pointer<std::remove_cvref_t<V>>)
struct matcher<Pattern, V> : smart_pointer_matcher<V, Pattern>
{
	using super_t = smart_pointer_matcher<V, Pattern>;

	template <bool Strict=true>
	constexpr bool match(auto &&p, V v) noexcept {
		return super_t::template match<Strict>(v, p);
	}

	constexpr decltype(auto) result(auto &&p, V v) const {
		return super_t::result(v, p);
	}
};

/// 5.3.2.2 Alternative Pattern
/// Case 3: Polymorphic Types
/// If Alt is a type and std::is_polymorphic_v<V> is true, let p be dynamic_cast<Alt'*>(&v)
/// where Alt' has the same cv-qualifications as decltype(&v). The alternative pattern
/// matches if p contextually converted to bool evaluates to true, and pattern matches *p.

/// type -> pointer matcher
/// Pointer must point to polymorphic class
template <typename Pattern, typename V>
	requires (detail::is_type<std::remove_cvref_t<Pattern>>
	&& std::is_pointer_v<std::remove_cvref_t<V>>
	&& std::is_polymorphic_v<std::remove_pointer_t<std::remove_cvref_t<V>>>)
struct matcher<Pattern, V>
{
	using target_type_t = typename Pattern::type_t;
	target_type_t *matched = nullptr;

	template <bool Strict=true>
	constexpr bool match([[maybe_unused]] const Pattern &p, auto &&v) noexcept {
		matched = dynamic_cast<target_type_t*>(v);
		return (matched != nullptr);
	}

	constexpr decltype(auto) result([[maybe_unused]] auto &&p, [[maybe_unused]] auto &&v) const noexcept {
		assert(matched != nullptr);
		return *matched;
	}
};

}} // namespace detail // namespace pm

#endif // PM_DETAIL_MATCH_POINTERS_HPP

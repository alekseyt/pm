#ifndef PM_EXT_SURELY_HPP
#define PM_EXT_SURELY_HPP

#include <cassert>
#include <concepts>
#include <utility>
#include <pm/detail/matcher.hpp>
#include <pm/ext/maybe.hpp>

namespace pm {
namespace detail {

/// 5.3.2.5 Dereference Pattern
/// The dereference pattern has the following forms:
/// --> (*!) pattern
///     (*?) pattern
/// The first form matches value v if pattern matches *v.
template <typename Subpattern>
struct surely
{
	using subpattern_t = Subpattern;

	explicit constexpr surely(Subpattern p)
	: subpattern(p)
	{}

	subpattern_t subpattern;
};

template <typename T>
concept is_surely = requires (T t) {
	{ pm::detail::surely{t} } -> std::same_as<T>;
};

/// surely matcher
template <typename Pattern, typename V>
	requires (detail::is_surely<std::remove_cvref_t<Pattern>>)
struct matcher<Pattern, V>
{
	using super_t = dereferencing_matcher_base<Pattern, V>;

	typename super_t::matcher_t matcher; // matcher to hold potential side effect

	// True if dereferenced value matches arg
	template <bool Strict=true>
		// value must be dereferencible
		requires (requires(V v) {
			{ *v };
		})
	constexpr bool match(const Pattern &surely, auto &&v) {
		return (this->matcher.match(surely.subpattern, *v));
	}

	/// Returns match result of subpattern with dereferenced value
	constexpr decltype(auto) result(const Pattern &surely, auto &&v) const {
		return matcher.result(surely.subpattern, *v);
	}
};

} // namespace detail

template <typename Pattern>
constexpr detail::is_surely auto surely(Pattern p) noexcept
{
	return detail::surely(p);
}

} // namespace pm

#endif // PM_EXT_SURELY_HPP

#ifndef PM_EXT_EXTRACT_HPP
#define PM_EXT_EXTRACT_HPP

namespace pm {
namespace detail {

// 5.3.2.6 Extractor Pattern
// The extractor pattern has the following two forms:
// --> ( constant-expression ! ) pattern
//     ( constant-expression ? ) pattern
//
// Let c be the constant-expression. The first form matches value v if pattern
// matches e where e is the result of a call to member c.extract(v) or else
// a non-member ADL-only extract(c, v).

template <typename Extractor, typename Subpattern>
struct extract_base
{
	using extractor_t = Extractor;
	using subpattern_t = Subpattern;

	extractor_t extractor;
	subpattern_t subpattern;
};

template <typename Extractor, typename Subpattern>
struct extract : extract_base<Extractor, Subpattern>
{
	using super_t = extract_base<Extractor, Subpattern>;

	explicit constexpr extract(Extractor e, Subpattern p)
	: super_t(e, p)
	{}
};

template <typename T>
concept is_extract = requires (T t) {
	{ pm::detail::extract{t} } -> std::same_as<T>;
};

/// Extract is compositing the other way around
/// Tentative extract is a closed extract (as opposed to open extract)
/// but can be reconstructed into another closed extract when combined
/// with another subpattern.
///
/// tentative_extract := extract + pm::any
/// tentative_extract + pattern := extract + pattern
///
/// Extra type is needed to unambiguously overload operator later
template <typename SuperExtract>
struct tentative_extract : SuperExtract
{
	using super_t = SuperExtract;

	explicit constexpr tentative_extract(SuperExtract se)
	: super_t(se)
	{}
};

template <typename T>
concept is_tentative_extract = requires (T t) {
	typename T::super_t;
	{ pm::detail::tentative_extract{t} } -> std::same_as<T>;
};

template <typename T>
concept is_unconditional_extract = detail::is_extract<T>
	|| (detail::is_tentative_extract<T> && detail::is_extract<typename T::super_t>);

} // namespace detail

namespace extract_bp {

template <typename Extractor, typename V>
concept extractor_with_member = requires (Extractor e, V v) {
	{ e.extract(v) }; // member version
};

template <typename Extractor, typename V>
concept extractor_with_adl = requires (Extractor e, V v) {
	{ extract(e, v) }; // adl version
};

// place extractor into different namespace so unqualified extract()
// doesn't clash with pm::detail::extract
template <typename T>
	requires (std::default_initializable<T>)
struct extractor
{
	T extractor;

	constexpr decltype(auto) perform_extract(auto &&v)
		requires (extract_bp::extractor_with_member<T, decltype(v)>)
	{
		return this->extractor.extract(std::forward<decltype(v)>(v));
	}

	constexpr decltype(auto) perform_extract(auto &&v)
		requires (extract_bp::extractor_with_adl<T, decltype(v)>)
	{
		return extract(this->extractor, std::forward<decltype(v)>(v));
	}
};

} // extract_bp

namespace detail {

/// Storage for actual extracted value e.g. extractor returns std::string
// (not a reference to std::string, actual std::string)
template <typename T>
struct value_storage
{
	using storage_t = value_storage<T>;

	T value;

	T& operator* () {
		return value;
	}

	storage_t& operator= (auto &&v) {
		this->value = std::forward<decltype(v)>(v);
		return *this;
	}
};

/// Storage for extractors that return a reference, including rvalue references.
/// Either reference is stored as lvalue reference ("materialized" so to speak).
/// Value can be moved later if stored reference isn't const.
/// However note that matcher's result() is invoked twice: first time when
/// pattern guard is checked - it accepts the same match result, second time when
/// match result is passed to callback. If pattern guard would move value, this
/// might backfire in callback.
///
/// All in all this looks like a decent footgun. To alleviate this, pattern guard
/// might enforce const-ness on arguments passed to it.
template <typename T>
	requires (std::is_reference_v<T>)
struct value_storage<T>
{
	using storage_t = value_storage<T>;
	using base_t = std::conditional_t<std::is_rvalue_reference_v<T>,
		std::remove_reference_t<std::remove_reference_t<T>>, std::remove_reference_t<T>>;

	base_t *value = nullptr;

	constexpr base_t& operator* () {
		return *value;
	}

	constexpr storage_t& operator= (auto &&v) {
		this->value = &v;
		return *this;
	}
};

/// Matcher for exract with unconditional extractor
template <typename Pattern, typename V>
	requires (detail::is_unconditional_extract<Pattern>)
struct matcher<Pattern, V>
{
	using extractor_t = extract_bp::extractor<typename Pattern::extractor_t>;
	using extractor_result_t = decltype(std::declval<extractor_t>().perform_extract(std::declval<V>()));

	bool matched = false;
	value_storage<extractor_result_t> extracted_value;

	/// Matches if subpattern matches extracted value
	template <bool Strict=true>
	constexpr bool match(const Pattern &extract, auto &&v) {
		auto matcher = detail::get_matcher<decltype(extract.subpattern), extractor_result_t>();

		extracted_value = extractor_t().perform_extract(v);
		matched = matcher.match(extract.subpattern, *extracted_value);

		return matched;
	}

	constexpr decltype(auto) result([[maybe_unused]] const Pattern &p, [[maybe_unused]] auto &&v) noexcept {
		assert(matched);
		return *extracted_value;
	}
};

} // namespace detail

template <typename Extractor, typename Subpattern>
constexpr detail::is_extract auto extract(Extractor e, Subpattern p) noexcept
{
	return detail::extract(e, p);
}

template <typename Extractor>
constexpr detail::is_tentative_extract auto extract(Extractor e) noexcept
{
	return detail::tentative_extract(detail::extract(e, pm::any()));
}

} // namespace pm

// here is a dirty trick though:
// open patterns are closed using operator `+` so this operator has to have
// higher precedence than `+`: https://en.cppreference.com/w/cpp/language/operator_precedence
// the choice is basically among `*`, `/` and `%`
// maybe variety can be extended if another operator is used instead of `+`
template <typename TentativeExtract, typename Subpattern>
	requires (pm::detail::is_tentative_extract<TentativeExtract>
	&& pm::detail::is_extract<typename TentativeExtract::super_t>)
constexpr pm::detail::is_extract auto operator% (TentativeExtract te, Subpattern p) noexcept
{
	return pm::detail::extract(te.extractor, p);
}

#endif // PM_EXT_EXTRACT_HPP

#ifndef PM_PASS_HPP
#define PM_PASS_HPP

#include <concepts>
#include <functional>
#include <pm/detail/adaptor.hpp>
#include <pm/detail/pass.hpp>

namespace pm {

/// Proposal says: 5.2 Basic Model
///
/// If ! prefix is used before compound statement - the statement would not contribute to return
/// type deduction for inspect expression. Such a statement is not expected to yield a value and
/// should stop the execution either by returning from the enclosing function, throwing an exception
/// or terminating the program. This allows users to express desired no-match behaviour or to act
/// upon broken invariant, without affecting return type of the whole of inspect expression.
///
/// It is not exactly that. Return value is ignored, if pattern matches, then callback is invoked,
/// but it's invoked as it would return void, then it's up to callback to throw exception, call
/// std::terminate() or do whatever.
template <typename Pattern, typename Callable>
constexpr detail::is_adaptor auto pass(Pattern p, Callable c) noexcept
{
	return detail::make_adaptor(std::make_tuple(detail::pass(p, c)));
}

} // namespace pm

#endif // PM_PASS_HPP

#include <cassert>
#include <memory>
#include <pm/pm.hpp>

namespace {

struct Shape
{
	virtual ~Shape() = default;
};

struct Circle : Shape
{
	float r = 7;
};

struct Rectangle : Shape
{
	int w = 5;
	int h = 11;
};

} // namespace

static_assert(std::is_polymorphic_v<Shape>);
static_assert(pm::detail::has_matcher<pm::type<Circle>, Shape *>);
static_assert(pm::detail::has_matcher<pm::type<Circle>, const Shape *>);
static_assert(pm::detail::has_matcher<pm::type<Circle>, Shape *&>);
static_assert(pm::detail::has_matcher<pm::type<Rectangle>, Shape *>);

void test_polymorphic_matching()
{
	std::unique_ptr<Shape> circle_ptr(new Circle());
	Shape *shape_ptr_1 = circle_ptr.get();

	assert(pm::match(shape_ptr_1
		| pm::with(pm::type<Circle>{}, [] { return true; })));
	assert(!pm::match(shape_ptr_1
		| pm::with(pm::type<Rectangle>{}, [] { return true; })));

	std::unique_ptr<const Shape> rectangle_ptr(new Rectangle());
	const Shape *shape_ptr_2 = rectangle_ptr.get();

	assert(!pm::match(shape_ptr_2
		| pm::with(pm::type<const Circle>{}, [] { return true; })));
	assert(pm::match(shape_ptr_2
		| pm::with(pm::type<const Rectangle>{}, [] { return true; })));
}

void test_polymorphic_callback()
{
	std::unique_ptr<const Shape> rectangle_ptr(new Rectangle());
	const Shape *shape_ptr = rectangle_ptr.get();

	const auto match = pm::match(shape_ptr
		| pm::with(pm::type<const Circle>{}, [] (const Circle &c) -> int { return 3.14 * c.r * c.r; })
		| pm::with(pm::type<const Rectangle>{}, [] (const Rectangle &r) { return r.w * r.h; }));
	assert(match && *match == 5 * 11);
}

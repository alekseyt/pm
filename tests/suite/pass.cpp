#include <pm/pm.hpp>

static_assert(pm::detail::is_clause<pm::detail::pass<int, decltype([]{})>, int, true>);
static_assert(pm::detail::is_clause<pm::detail::pass<int, decltype([]{})>, int, false>);

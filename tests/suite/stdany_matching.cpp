#include <any>
#include <cassert>
#include <pm/ext/stdany.hpp>
#include <pm/pm.hpp>

static_assert(pm::detail::has_matcher<pm::type<int>, std::any>);
static_assert(pm::detail::has_matcher<pm::type<int>, std::any&>);
static_assert(pm::detail::has_matcher<pm::type<int>, const std::any&>);
static_assert(pm::detail::has_matcher<pm::type<int>, std::any&&>);

void test_stdany_match_types()
{
	std::any a = 10;
	assert(pm::match(a
		| pm::with(pm::type(int{}), [] { return true; })));
	assert(!pm::match(a
		| pm::with(pm::type(float{}), [] { return true; })));

	a = 11.5;
	assert(!pm::match(a
		| pm::with(pm::type(int{}), [] { return true; })));
	assert(!pm::match(a
		| pm::with(pm::type(float{}), [] { return true; })));
	assert(pm::match(a
		| pm::with(pm::type(double{}), [] { return true; })));
}

// check that value of corresponding type is passed to callback
// when any matches a type
void test_stdany_match_callback()
{
	std::any a = 11;
	const auto match = pm::match(a
		| pm::with(pm::type(int{}), [] (int x) { return x * 2; }));
	assert(match && *match == 22);
}

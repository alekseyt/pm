#ifndef PM_EXT_DECONS_HPP
#define PM_EXT_DECONS_HPP

#include <concepts>
#include <tuple>
#include <pm/detail/concepts.hpp>
#include <pm/detail/dispatcher.hpp>

namespace pm {
namespace detail {

struct open_decons
{};

template <typename T>
concept is_open_decons = requires (T t) {
	{ pm::detail::open_decons{t} } -> std::same_as<T>;
};

// rebuild tuple-like into a tuple of const references
template <std::size_t I, typename T>
	requires (detail::is_tuple_like<std::remove_cvref_t<T>>
	&& I < std::tuple_size_v<T>)
constexpr detail::is_tuple auto rebuild_as_const_tuple(const T &t)
{
	// const reference to what is stored in tuple-like
	const auto &const_ref = std::get<I>(t);

	// build tuple of const references
	if constexpr (I+1 < std::tuple_size_v<T>) {
		return std::tuple_cat(std::forward_as_tuple(const_ref), rebuild_as_const_tuple<I+1>(t));
	}
	else {
		return std::forward_as_tuple(const_ref);
	}
}

/// Wrap match result into dedicated type
/// to unambiguously specialize detail::dispatcher later
template <typename T, typename StoredT=std::remove_cvref_t<T>>
	requires (detail::is_tuple_like<StoredT>)
struct decons : open_decons
{
	explicit constexpr decons(T &&result)
	: open_decons()
	, result(std::move(result))
	{}

	const StoredT& operator* () const {
		return result;
	}

	auto make_const() const {
		return detail::rebuild_as_const_tuple<0>(result);
	}

	StoredT result;
};

template <typename T>
concept is_decons = requires (T t) {
	{ pm::detail::decons{t} } -> std::same_as<T>;
};

/// Specialize detail::dispatcher for detail::decons
/// to use std::apply() instead of std::invoke()
template <typename Pattern, typename Callable, typename V>
	requires (detail::is_decons<std::remove_cvref_t<V>>)
struct dispatcher<Pattern, Callable, V>
{
	/// "Unroll" result stored in decons into a list of arguments
	decltype(auto) dispatch(const Callable &c, auto &&v) {
		// while *v returns const reference to value stored in v,
		// normally result of tuples matching is std::tuple(&, &, &, ...)
		// so it's a const reference to tuple of non-const references
		// and std::apply can invoke callable with non-const arguments
		return std::apply(c, *v);
	}

	/// V is detail::decons, but callback doesn't take arguments
	decltype(auto) dispatch(const Callable &c, [[maybe_unused]] auto &&v)
		requires (std::invocable<Callable>)
	{
		return std::invoke(c);
	}

	decltype(auto) const_dispatch(const Callable &c, const auto &v) {
		return std::apply(c, v.make_const());
	}

	decltype(auto) const_dispatch(const Callable &c, [[maybe_unused]] const auto &v)
		requires (std::invocable<Callable>)
	{
		return dispatch(c, v);
	}
};

// TODO: matcher for decons is a good idea or nah?

} // namespace detail

template <typename T>
constexpr detail::decons<T> decons(T &&t) noexcept
{
	return detail::decons<T>(std::forward<T>(t));
}

constexpr detail::open_decons decons() noexcept
{
	return {};
}

} // namespace pm

#endif // PM_EXT_DECONS_HPP

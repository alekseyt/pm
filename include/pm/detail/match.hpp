#ifndef PM_DETAIL_MATCH_HPP
#define PM_DETAIL_MATCH_HPP

#include <pm/detail/match_pointers.hpp>
#include <pm/detail/match_types.hpp>
#include <pm/detail/match_tuples.hpp>
#include <pm/detail/match_values.hpp>
#include <pm/detail/match_variant.hpp>
#include <pm/detail/matcher.hpp>

#endif // PM_DETAIL_MATCH_HPP

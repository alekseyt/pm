#include <cassert>
#include <pm/ext.hpp>
#include <pm/pm.hpp>

static_assert(pm::detail::has_matcher<pm::detail::conj<int, int>, int>);
static_assert(pm::detail::has_matcher<pm::detail::conj<bool, bool>, bool>);
static_assert(pm::detail::has_matcher<pm::detail::conj<float, float>, float>);

void test_conj()
{
	assert(pm::match(10
		| pm::with(pm::conj(10, 10), [] { return true; })));
	assert(!pm::match(10
		| pm::with(pm::conj(10, 11), [] { return true; })));
	assert(!pm::match(10
		| pm::with(pm::conj(11, 10), [] { return true; })));
}

void test_open_conj()
{
	assert(pm::match(10
		| pm::with(10 + pm::conj(10), [] { return true; })));
	assert(!pm::match(10
		| pm::with(10 + pm::conj(11), [] { return true; })));
	assert(!pm::match(10
		| pm::with(11 + pm::conj(10), [] { return true; })));
}

// Test conjuction conjuction
void test_conj_conj()
{
	assert(pm::match(10
		| pm::with(10 + pm::conj(10), [] { return true; })));
	assert(pm::match(10
		| pm::with(10 + pm::conj(10) + pm::conj(10), [] { return true; })));
	assert(!pm::match(10
		| pm::with(10 + pm::conj(10) + pm::conj(11), [] { return true; })));
	assert(!pm::match(10
		| pm::with(10 + pm::conj(11) + pm::conj(10), [] { return true; })));
	assert(!pm::match(10
		| pm::with(11 + pm::conj(10) + pm::conj(10), [] { return true; })));
}

#ifndef PM_DETAIL_PASS_HPP
#define PM_DETAIL_PASS_HPP

#include <pm/detail/clause.hpp>
#include <pm/detail/concepts.hpp>
#include <pm/detail/dispatcher.hpp>
#include <pm/detail/matcher.hpp>

namespace pm { namespace detail {

/// pass clause
template <typename Pattern, typename Callable>
struct pass : detail::Clause<Pattern, Callable, true>
{
	using super_t = detail::Clause<Pattern, Callable, true>;

	explicit constexpr pass(Pattern pattern, Callable c)
	: super_t(pattern, c)
	{}

	/// If pattern matches - invoke callback with match result
	/// Always returns void
	template <bool Strict=true, typename V,
		typename MatcherType=decltype(detail::get_matcher<Pattern, V>()),
		typename MatcherResultType=decltype(std::declval<MatcherType>().result(
			std::declval<Pattern>(), std::declval<const V &>())),
		typename DispatcherType=decltype(detail::get_dispatcher<Pattern, Callable, MatcherResultType>())>
		requires (detail::can_match_pattern_with_v<MatcherType, Pattern, V, Strict>
		&& detail::matcher_result_fits_callback_args<DispatcherType, Callable, MatcherResultType>)
	constexpr detail::is_eval_result auto evaluate(V &&v) const
	{
		MatcherType matcher;

		if (!matcher.template match<Strict>(this->pattern, v)) {
			return detail::EvalResultVoid{ false };
		}

		DispatcherType{}.dispatch(this->callback, matcher.result(this->pattern, v));

		return detail::EvalResultVoid{ true };
	}
};

}} // namespace detail // namespace pm

#endif // PM_DETAIL_PASS_HPP

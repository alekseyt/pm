#ifndef PM_DETAIL_MATCH_VARIANT_HPP
#define PM_DETAIL_MATCH_VARIANT_HPP

#include <cassert>
#include <concepts>
#include <pm/detail/concepts.hpp>
#include <pm/detail/matcher.hpp>
#include <pm/type.hpp>

namespace pm { namespace detail {

// 5.3.2.2 Alternative Pattern
// Case 1: std::variant-like
// If std::variant_size_v<V> is well-formed and evaluates to an integral, the alternative
// pattern matches v if Alt is compatible with the current index of v and pattern matches
// the active alternative of v.

// Checks that value can potentially match one of variant alternatives
template <std::size_t I, typename Variant, typename V>
	requires (detail::is_variant<std::remove_cvref_t<Variant>>
	&& I < std::variant_size_v<std::remove_cvref_t<Variant>>)
consteval bool can_match_variant_value()
{
	using variant_t = std::remove_cvref_t<Variant>;

	if constexpr (detail::has_matcher<std::variant_alternative_t<I, variant_t>, V>) {
		return true;
	}

	if constexpr (I+1 < std::variant_size_v<variant_t>) {
		return can_match_variant_value<I+1, Variant, V>();
	}
	else {
		return false;
	}
}

/// Base class for variant -> value and value -> variant matchers
template <typename Pattern, typename V>
	requires (detail::is_variant<std::remove_cvref_t<Pattern>>)
struct variant_matcher_value
{
	bool matched = false;

	/// True if variant value matches value
	template <bool Strict=true>
	constexpr bool match(const Pattern &p, auto &&v)
		requires (!Strict || detail::can_match_variant_value<0, Pattern, V>())
	{
		matched = std::visit([this, &v] (auto &&pval) {
			if constexpr (detail::has_matcher<decltype(pval), decltype(v)>) {
				if (detail::get_matcher(pval, v).match(pval, v)) {
					return true;
				}
			}

			return false;
		}, p);

		return matched;
	}

	constexpr decltype(auto) result([[maybe_unused]] auto &&p, auto &&v) const noexcept {
		assert(matched);
		return std::forward<decltype(v)>(v);
	}
};

/// Matcher for variant (match values)
template <typename Pattern, typename V>
	requires (detail::is_variant<std::remove_cvref_t<Pattern>>
	&& !detail::is_variant<std::remove_cvref_t<V>>
	&& !detail::explicit_variant_matching<std::remove_cvref_t<V>>)
struct matcher<Pattern, V> : variant_matcher_value<Pattern, V> {};

/// Matcher for values (match variant)
template <typename Pattern, typename V>
	requires (detail::is_variant<std::remove_cvref_t<V>>
	&& !detail::is_variant<std::remove_cvref_t<Pattern>>
	&& !detail::explicit_variant_matching<std::remove_cvref_t<Pattern>>)
struct matcher<Pattern, V> : variant_matcher_value<V, Pattern> {
	using super_t = variant_matcher_value<V, Pattern>;

	template <bool Strict=true>
	constexpr bool match(auto &&p, const V &v) {
		// swap arguments to call base::match() correctly
		return super_t::template match<Strict>(v, p);
	}

	constexpr decltype(auto) result(auto &&p, auto &&v) const noexcept {
		return super_t::result(std::forward<decltype(v)>(v),
			std::forward<decltype(p)>(p));
	}
};

/// Base class for variant -> type and type -> variant matchers
template <typename Pattern, typename V>
	requires (detail::is_variant<std::remove_cvref_t<Pattern>>)
struct variant_matcher_type
{
	bool matched = false;

	/// True if stored value type matches value (which is type)
	template <bool Strict=true>
	constexpr bool match(const Pattern &p, auto &&v) {
		matched = std::visit([this, &v, &p] (auto &&pval) {
			if (!detail::get_matcher(pm::type(pval), v).match(pm::type(pval), v)) {
				return false;
			}

			return true;
		}, p);

		return matched;
	}

	constexpr decltype(auto) result(const Pattern &p, [[maybe_unused]] auto &&v) const noexcept {
		assert(matched);
		return p; // XXX: this is different from matching values (match result swapped)
	}
};

/// Matcher for variant (match types)
template <typename Pattern, typename V>
	requires (detail::is_variant<std::remove_cvref_t<Pattern>>
	&& detail::is_type<std::remove_cvref_t<V>>)
struct matcher<Pattern, V> : variant_matcher_type<Pattern, V> {};

/// Matcher for types (match variant)
template <typename Pattern, typename V>
	requires (detail::is_type<std::remove_cvref_t<Pattern>>
	&& detail::is_variant<std::remove_cvref_t<V>>)
struct matcher<Pattern, V> : variant_matcher_type<V, Pattern>
{
	using super_t = variant_matcher_type<V, Pattern>;

	template <bool Strict=true>
	constexpr bool match(auto &&p, const V &v) {
		return super_t::template match<Strict>(v, p);
	}

	constexpr decltype(auto) result(auto &&p, auto &&v) const noexcept {
		return super_t::result(std::forward<decltype(v)>(v),
			std::forward<decltype(p)>(p));
	}
};

/// Match variant to variant
template <typename Pattern, typename V>
	requires (detail::is_variant<std::remove_cvref_t<Pattern>>
	&& detail::is_variant<std::remove_cvref_t<V>>)
struct matcher<Pattern, V>
{
	bool matched = false;

	/// True when both type and value match
	template <bool Strict=true>
	constexpr bool match(const Pattern &p, const V &v) {
		matched = std::visit([this, &v] (auto &&pval) {
			return std::visit([this, &v, &pval] (auto &&vval) {
				if constexpr (detail::has_matcher<decltype(pval), decltype(vval)>) {
					if (detail::get_matcher(pm::type(pval), pm::type(vval)).match(pm::type(pval), pm::type(vval))
					&& detail::get_matcher(pval, vval).match(pval, vval)) {
						return true;
					}
				}

				return false;
			}, v);
		}, p);

		return matched;
	}

	constexpr decltype(auto) result([[maybe_unused]] auto &&p, auto &&v) const noexcept {
		assert(matched);
		return v;
	}
};

}} // namespace detail // namespace pm

#endif // PM_DETAIL_MATCH_VARIANT_HPP

#ifndef PM_TYPE_HPP
#define PM_TYPE_HPP

namespace pm {

/// Types that can be passed to functions as arguments
/// e.g. f(Type<int>{}, g(Type<string>{}))
/// Type(10) works too
template <typename T>
struct Type
{
	using explicit_variant_matching = std::true_type;

	using type_t = T;

	explicit constexpr Type() noexcept = default;
	explicit constexpr Type(T) noexcept {}
};

/// Snake-case alias for Type
template <typename T>
using type = Type<T>;

namespace detail {

template <typename T>
concept is_type = requires (T t) {
	{ pm::Type{t} } -> std::same_as<T>;
};

}} // namespace detail // namespace pm

#endif // PM_TYPE_HPP

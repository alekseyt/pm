#include <cassert>
#include <pm/ext.hpp>
#include <pm/pm.hpp>

static_assert(pm::detail::has_matcher<pm::detail::neg<int>, int>);
static_assert(pm::detail::has_matcher<pm::detail::neg<bool>, bool>);
static_assert(pm::detail::has_matcher<pm::detail::neg<float>, float>);

void test_neg()
{
	assert(pm::match(10
		| pm::with(10, [] { return true; })));
	assert(!pm::match(10
		| pm::with(pm::neg(10), [] { return true; })));
	assert(pm::match(10
		| pm::with(pm::neg(11), [] { return true; })));
}

void test_neg_open_neg()
{
	assert(pm::match(10
		| pm::with(10, [] { return true; })));
	assert(!pm::match(10
		| pm::with(10 + pm::neg(), [] { return true; })));
	assert(pm::match(10
		| pm::with(11 + pm::neg(), [] { return true; })));
}

void test_neg_any_of()
{
	assert(pm::match(9
		| pm::with(pm::neg(pm::any_of(10, 11, 12)), [] { return true; })));
	assert(!pm::match(11
		| pm::with(pm::neg(pm::any_of(10, 11, 12)), [] { return true; })));

	// open neg
	assert(pm::match(9
		| pm::with(pm::any_of(10, 11, 12) + pm::neg(), [] { return true; })));
	assert(!pm::match(11
		| pm::with(pm::any_of(10, 11, 12) + pm::neg(), [] { return true; })));
}

#include <cassert>
#include <string>
#include <pm/ext.hpp>
#include <pm/pm.hpp>

static_assert(pm::detail::has_matcher<pm::detail::cons<std::tuple<int>, std::function<int(std::tuple<int>)>>, std::tuple<int>>);

void test_cons()
{
	struct S
	{
		int x = 10;
		std::string s = "abc";
	} const s = {
		.x = 11,
		.s = "cba",
	};

	const auto match = pm::match(std::tie(s.x, s.s)
		| pm::with(pm::cons(std::make_tuple(pm::__, pm::__), [] (const auto &tuple) {
			return pm::decons(std::tie(std::get<1>(tuple)));
		}), [] (const std::string &s) {
			return s;
		}));
	assert(match && *match == "cba");

	assert(!pm::match(std::tie(s.x, s.s)
		| pm::with(pm::cons(std::make_tuple(42, "zzz"), [] (const auto &tuple) {
			return tuple;
		}), [] {
			return true;
		})));
}

// Check that cons can be used with callback with no args
void test_cons_no_args_callback()
{
	assert(pm::match(std::make_tuple(11, "abc")
		| pm::with(pm::cons(std::make_tuple(pm::__, pm::__), [] (const auto &tuple) {
			return tuple;
		}), [] {
			return true;
		})));
}

// Composition with maybe
void test_cons_with_maybe()
{
	struct S
	{
		int y = 11;
		std::string z = "zzz";
	} const s;

	const auto match = pm::match(&s
		| pm::with(pm::cons(pm::maybe(pm::__), [] (const S &s) {
			return pm::decons(std::tie(s.y, s.z));
		}), [] (int x, std::string) {
			return x * 2;
		}));
	assert(match && *match == 22);
}

// Check that pm::decons is really optional
void test_cons_no_decons()
{
	struct S
	{
		int y = 11;
		std::string z = "zzz";
	} const s;

	const auto match = pm::match(&s
		| pm::with(pm::cons(pm::maybe(pm::__), [] (const S &s) {
			return s; // pass through structure to callback
		}), [] (const S &s) {
			return s.y * 3;
		}));
	assert(match && *match == 33);
}

// Multi-level composition of patterns
void test_cons_with_surely_any()
{
	// XXX: this is actually not ideal when additional comments
	// required to mark where is which callback
	const int x = 11;
	const auto match = pm::match(&x
		| pm::with(pm::cons(pm::surely(pm::__), [] (int x) { // cons
			return pm::decons(std::make_tuple(x, std::to_string(x)));
		}), [] (int x, const std::string &s) { // actual match callback
			return (std::to_string(x) + s);
		}));
	assert(match && *match == "1111");
}

void test_cons_open_cons()
{
	const auto match_1 = pm::match(10
		| pm::with(pm::__
			+ pm::cons([] (int x) { return std::to_string(x * 3); }),
			[] (const std::string &s) { return s + s; }));
	assert(match_1 && *match_1 == "3030");

	// native type subpattern
	const auto match_2 = pm::match(11
		| pm::with(11
			+ pm::cons([] (int x) { return pm::decons(std::make_tuple(x * 2, x * 3)); }),
			[] (int x1, int x2) { return x1 * 100 + x2; }));
	assert(match_2 && *match_2 == 2233);
}

// Same multi-level test as above, but with open cons
void test_cons_open_cons_with_surely_any()
{
	const int x = 11;
	const auto match = pm::match(&x
		| pm::with(pm::surely(pm::__)
			+ pm::cons([] (int x) {
				return pm::decons(std::make_tuple(x, std::to_string(x)));
			}),
			[] (int x, const std::string &s) {
				return (std::to_string(x) + s);
			})
	);
	assert(match && *match == "1111");
}

// Check that cons composites with cons
void test_cons_with_cons()
{
	const auto match = pm::match(std::make_tuple(10, "abc")
		| pm::with(pm::__
			+ pm::cons([] (const auto &t) { return pm::decons(t); })
			+ pm::cons([] (int x, const std::string &s) { return pm::decons(std::make_tuple(s, x * 2)); })
			+ pm::cons([] (const std::string &s, int x) { return pm::decons(std::make_tuple(x * 3, s)); }),
			[] (int x, const std::string &s) { return s + std::to_string(x); })
	);
	assert(match && *match == "abc60");
}

void test_cons_open_decons()
{
	const auto match = pm::match(std::make_tuple(10, 11)
		| pm::with(std::make_tuple(pm::__, pm::__)
			+ pm::decons(),
			[] (int x, int y) { return x + y; }));
	assert(match && *match == 21);
}

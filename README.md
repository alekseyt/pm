[TOC]

## What is this

Proof-of-concept pattern matching for C++20.

```c++
match(x
  | with(0,  [] { return "Zero"; })
  | with(1,  [] { return "One"; })
  | with(2,  [] { return "Two"; })
  | with(3,  [] { return "Three"; })
  | with(__, [] { return "Other"; }))
```

## Why another pattern matching library

It's always good to check that C++ can be potentially implemented in C++
instead of hacking compiler for every cool feature. Although language-supported
feature might be superior.

Other pattern matching libraries:

* [Mach7][]
* [patterns][]
* [SimpleMatch][]

## Status

It's a work in progress and very experimental.

## Compilers support

* GCC 10.2

## Relation to P1371

This library does not in any way represent [P1371][] or anyone or
anything related to it. This is completely independent implementation
approaching the same problem from the different perspective.

But it does implement similar feature set in different fashion.
P1371 is "Language rather than Library", this is, obviously, quite
the opposite.

## Design

On base level pm implements:

* `pm::match(expression)` - evaluate expression

Next two are clauses:

* `pm::with(pattern, callback)` or `pm::with(pattern, guard, callback)` -
  match with pattern and return if value matched
* `pm::pass(pattern, callback)` - match with pattern but fall through to the
  next clause regardless

The rest are technically customization points mostly in form of matchers
that can match values (or patterns) of specific types. This customization
point is similar to [SimpleMatch][] matchers.

Default matcher is for values that can be compared with `operator==`. To
compare types `pm::type()` can be used for which matcher is also provided.

```c++
match(pm::type(x)
  | with(pm::type(int{}), [] {}));
```

Matchers for pointers, `std::tuple` and `std::variant` are also a part of
the core package.

The rest are some of the patterns suggested by [P1371][]:

* `pm::any()` and `pm::__` - matches with anything
* `pm::any_of(x, y, z, ...)` and `pm::any_of(std::ranges::range)` - matches
  with any value from the list
* `pm::within(from, to)` and `pm::within(std::pair)` - matches value with range
* `pm::maybe(subpattern)` - conditionally dereference value that is being
  matched and match it with pattern
* `pm::surely(subpattern)` - unconditionally dereference value that is being
  matched and match it with pattern
* `pm::extract(extractor, subpattern)` - unconditionally extracts value and
  match it with subppatern
* `pm::try_extract(extractor, subpattern)` - conditionally extracts value and
  match it with subppatern

And due to popular demand:

* `pm::cons(subpattern, constructor)` - wrap subpattern into constructor
  function and transform matched value into something else, then pass that
  to callback
* `pm::decons(tuple-like)` and `pm::decons()` - if tuple is wrapped into
  `pm::decons`, it's going to be expanded into list of arguments when passed
  to callback. `pm::decons` is not a pattern, but special type to overload
  value dispatching to callback

But my personal favorites are:

* `pm::neg(pattern)` - negates (logical-not) match result of a pattern
* `pm::conj(pattern_left, pattern_right)` - conjuction (logical-and) of two patterns
* `pm::disj(pattern_left, pattern_right)` - disjunction (logical-or) of two patterns

### Crash course

Expression of type `pm::detail::Expression` has structure like so:

```lisp
(x (
  (pass (pattern callback))
  (with (pattern callback))
))
```

It's a value to be matched + list of clauses and satisfies
`pm::detail::is_expression` concept. List of clauses is `pm::detail::Adaptor`:

```lisp
(
  (pass (pattern callback))
  (with (pattern callback))
)
```

which satisfies `pm::detail::is_adaptor` concept. A clause:

```lisp
  (pass (pattern callback))
```

is a pattern + callback and must satisfy `pm::detail::is_clause` concept.

Pattern is any type including built-in types like `int` or `const char*`, or
`pm::detail::any_of` if that matters. pm will select corresponding matcher
automagically or code won't compile if there is no suitable matcher. Actually,
matcher is selected by compiler from `struct pm::detail::matcher`
specializations and must fit `pm::detail::has_matcher` concept when
instantiated for some unspecified type. Failure to fit leads to compile error.

In pm clause look like this:

```c++
  pm::pass(pattern, [] {})
```

To create adaptor using overloaded `|` operator:

```c++
  pm::pass(pattern, [] {})
| pm::with(pattern, [] {})
```

To create expression:

```c++
(x
| pm::pass(pattern, [] {})
| pm::with(pattern, [] {}))
```

And then pass to `pm::match()`:

```c++
pm::match(x
  | pm::pass(pattern, [] {})
  | pm::with(pattern, [] {}));
```

### Patterns compositing

Normally nested patterns have structure like so:

```lisp
(cons subpattern constructor)
```

This is OK, but doesn't look very pretty or intuitive when nested patterns
pile up:

```lisp
(cons (surely (any))
      constructor)
```

To "flatten" and "unroll" nested patterns, in some cases, pm overloads `+`
operator.

```c++
pm::surely(pm::__)
  + pm::cons([] (auto x) { return x; })
```

is equivalent to

```c++
pm::cons(pm::surely(pm::__),
  [] (auto x) { return x; })
```

`pm::cons(constructor)` without subpattern creates an *open* cons pattern
that need to be *closed* with subpattern:

```c++
pm::__ + pm::cons(constructor) // close cons with pm::__ subpattern
```

now cons is closed and can be used in `pm::with()` or as a subpattern in
other pattern. This is equivalent to:

```c++
pm::cons(pm::__, constructor) // create already closed cons
```

#### pm::extract

Extract is a bit different.

`pm::extract(extractor)` creates *tentative* pattern that matches `pm::any()`.
It can be used in `pm::with` right away, but also can be overriden with another
subpattern like so:

```c++
pm::extract(phone_number{}) % std::make_tuple("415", pm::__, pm::__)
```

This *seals* extract and makes it into a normal regular `pm::extract`
equivalent to

```c++
pm::extract(phone_number{}, std::make_tuple("415", pm::__, pm::__))
```

The goal of this is to have both `pm::extract` with default value that matches
anything and ability to pass pattern for nested matching without introducing
nesting. Another operator is used to remove ambiguity, for example:

```c++
pm::extract(phone_number{}) + std::make_tuple("415", pm::__, pm:__)
  + pm::decons()
```

Is not quite clear how to compute for both human and compiler.

```c++
pm::extract(phone_number{}) % std::make_tuple("415", pm::__, pm:__)
  + pm::decons()
```

Now compiler (and hopefully human) can understand that `std::make_tuple`
is a pattern that has to be matched against value extracted by `phone_number{}`
and that must be deconstructed into components.

Another example of ambiguity if `+` would be used:

```c++
pm::extract(phone_number{}) + pm::decons()
```

### Matchers and dispatchers machinery

TODO

## So what?

The following code results in compile error because pm "sees" that string is
matched to integers, match result is always `false`, so pm is disabling matcher
for value `"abc"` and pattern `any_of(1, 2, 3)`.

```c++
pm::match("abc"
  | pm::with(pm::any_of(1, 2, 3), [] {}));
```

```
../include/pm/ext/any_of.hpp:79:21: note: no operand of the disjunction is satisfied
   79 |   requires (!Strict || detail::can_match_any_of_value<0, Pattern, V>())
      |            ~~~~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

Following is not a compile error because matcher isn't disabled, although it
won't match:

```c++
pm::match("abc"
  | pm::with(pm::any_of(1, 2, "3"), [] {}));
```

This behavior can be avoided by unsettings `Strict` flag in `pm::match()` at
compile time:

```c++
pm::match<false>("abc"
  | pm::with(pm::any_of(1, 2, 3), [] {}));
```

## Comparison with P1371 (cookbook)

This might or might not be up to date with latest revision of [P1371][] which
was R3 by the time of writing.

### 5.3.1 Primary Patterns
### 5.3.1.1 Wildcard Pattern
### 5.3.1.2 Identifier Pattern
### 5.3.1.3 Expression Pattern

```c++
match(x
  | with(0,  [] { return "Zero"; })
  | with(1,  [] { return "One"; })
  | with(2,  [] { return "Two"; })
  | with(3,  [] { return "Three"; })
  | with(__, [] { return "Other"; }))
```

### 5.3.2 Compound Patterns
### 5.3.2.1 Structured Binding Pattern

```c++
struct S {
  int x = 42;
  string s;
} const s;

match(tie(s.x, s.s)
  | with(make_tuple(42, __) + decons(),
    [] (int x, const string &y) { return to_string(x) + y; }));
```

### 5.3.2.2 Alternative Pattern

> Case 1: std::variant-like

```c++
variant<string, int> v = 10;

match(v
  | with(pm::any_of(10, "abc"), [] (auto) {}));
```
> Case 2: std::any-like

> Case 3: Polymorphic Types

```c++
struct Shape { virtual ~Shape() = default; };
struct Circle : Shape { float r = 7; };
struct Rectangle : Shape { int w = 5; int h = 11; };

unique_ptr<Shape> rectangle_ptr(new Rectangle());
Shape *shape_ptr = rectangle_ptr.get();

match(shape_ptr
  | with(pm::type<Circle>{}, [] (Circle &c) -> int {
    return 3.14 * c.r * c.r;
  })
  | with(pm::type<Rectangle>{}, [] (Rectangle &r) {
    return r.w * r.h;
  }));
```

### 5.3.2.3 Parenthesized Pattern

```c++
struct Point { int x; int y; };
struct Line { Point from; Point to; };

std::variant<Point, Line> v = Point{0, 0};
match(v
  | with(pm::type<Point>{}
    + cons([] (auto &v) { return get<Point>(v); }),
    [] (const Point &p) { return p.x + p.y; }));
```

### 5.3.2.4 Case Pattern

N/A?

### 5.3.2.5 Dereference Pattern

```c++
struct Node {
  int value;
  std::unique_ptr<Node> lhs, rhs;
} node;

match(tie(node.value, node.lhs)
  | with(make_tuple(__, nullptr) + decons(),
    [] (int /*value*/, std::unique_ptr<Node> &/*lhs*/) {})
  | with(make_tuple(__, maybe(__)) + decons(),
    [] (int /*value*/, Node &/*lhs*/) {})
);
```

### 5.3.2.6 Extractor Pattern

Extractor must be `std::default_initializable`, it will be instantiated at the
time of matching.

Conditional:

```c++
struct String {
  optional<string> try_extract(int x) {
    return make_optional(to_string(x));
  }
};

match(10
  | with(try_extract(String{}), [] (const string &) {}));
```

Unconditional:

```c++
struct Int {
  int extract(const string &s) {
    return atoi(s.c_str());
  }
};

match("10"
  | with(extract(Int{}), [] (int) {}));
```

### 5.4 Pattern Guard

```c++
match(10 | with(__,
  [] (int x) { return x <= 11; }, // guard
  [] { return true; })); // callback
```

TODO

## Optimizations, Exhaustiveness and Usefulness

Not in the scope of this implementation.

However note the `Strict` flag in `match()`.

## Issues

### Value -> any matching and overload resolution problem (e.g. variant -> any value)

~~This is the issue related to overloads resolution when matcher for
`std::variant` can potentially match any value. So does some other matcher,
namely `pm::any`. When matcher is being resolved, sometimes ambiguity
appears when both matchers can match each other by value.~~

~~To resolve ambiguity one matcher is need to be "narrowed" in a sense,
more specifically an overload for `pm::any` that matches `std::variant`
provided so there is specific matcher that links `pm::any` and
`std::variant` in unambiguous way.~~

~~This is less than ideal because it couples `pm::any` and `std::variant`.~~

Recently matchers gained ability to define `explicit_variant_matching` typename
to disable implicit matching with `std::variant`. Surprisingly this solved more
than one issue:

1. Matcher resolution isn't ambiguous anymore.
2. Matchers generally won't have to have specialization for variant because
   compound pattern would deconstruct itself into components and match
   every component with variant. Components usually have trivial type and
   default matcher for variant can be used.

On the other hand, matchers now need to define typename if matching with
variant is not desirable, and specialize for matching variant if they can't be
deconstructed into trivial values.

### Side effect propagation from extractor to matcher

~~Matcher's `result()` method is `const`, therefore it returns enclosed side
effect (extracted value) as const reference, therefore result of extractor's
`extract()` or `try_extract()` is implicitly const too.~~

~~If `const` is removed from matcher's `result()`, it must be possible
to pass non-const extractor's result to callback, but if callback create side
effect on extracted value, this would propagate this side effect back to
matcher (that hold extracted value) which might or might not be ideal.~~

Yeah, whatever.

```c++
struct PimpMyString {
  std::string& extract(std::string &s) {
    return s;
  }
};

std::string s;
match(s
  | with(extract(PimpMyString{}),
    [] (string &s) { s = "oof"; }));

std::cout << s << std::endl;
```

Actually, in the example above, side effect is propagated back to value being
matched. Here is example of propagating side effect to matcher:

```c++
struct HoldMyString {
  std::string extract(const std::string &s) {
    return s;
  }
};

s = *match(std::string{}
  | with(extract(HoldMyString{}),
    [] (string &s) {
      s = "pff";
      return s;
    }));

std::cout << s << std::endl;
```

To be entirely honest, this isn't specific to extractors, but extractors were
so kind to uncover this phenomen. Minimal example of side effect propagation:

```c++
match(s | with(__, [] (string &s) { s = "tootooroo"; }));

std::cout << s << std::endl;
```

All examples pass Valgrind check.

## Things to look into

* 4.6 Evaluating Expression Trees
* Custom matchers and error reporting by compiler. More specifically:
    * ~~error reporting when match callback doesn't fit arguments that are
      going to be passed to it~~
    * ~~error reporting when fitting matcher can not be instantiated
      (read: match() can not be called on value being matched)~~
    * concept for checking that clauses return type is the same
      across one match expression question mark?
    * -fconcepts-diagnostics-depth=
* Sensible example
* [P1371][]
* [The Common Lisp Cookbook – Pattern Matching](https://lispcookbook.github.io/cl-cookbook/pattern_matching.html)

## Open questions (wrt P1371)

* What case pattern (5.3.2.4) is for? I don't get it, what problem does
  it solve?
* `at` and `both` purpose is unclear

## Incomplete collection of edge cases

* `"abc"` is dereferencible and convertible to `bool` so it looks like it fits
  both `pm::surely` and `pm::maybe`, but dereferencing it would produce `'a'`

## Assorted implementation notes

* `pm::decons()` is implemented as open cons (pseudocode):
  `cons([] (auto whatever) { return decons(whatever); }`.
  The idea is to wrap `whatever` into `pm::decons` for corresponding dispatcher
  to kick in.
* While extractors can be implemented in pm, specializing matchers feels like
  a more natural way of extending. Perhaps extractors fit P1371 better
  than pm. With that said, ADL has a very interesting synergy with pattern
  matching and probably should be explored further.

## Later

* Doxygen doc
* Modularize

## References

* P1371: https://wg21.link/P1371

## Questions?

[aleksey.tulinov@gmail.com](mailto:aleksey.tulinov@gmail.com)

[P1371]: https://wg21.link/P1371
[Mach7]: https://github.com/solodon4/Mach7
[patterns]: https://github.com/mpark/patterns
[SimpleMatch]: https://github.com/jbandela/simple_match

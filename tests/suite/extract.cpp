#include <array>
#include <cassert>
#include <charconv>
#include <regex>
#include <pm/ext.hpp>
#include <pm/pm.hpp>

namespace {

struct StaticPassThrough
{
	template <typename T>
	T extract(T t) {
		return t;
	}
};

struct OptionalPassThrough
{
	template <typename T>
	std::optional<T> try_extract(T t) {
		return std::make_optional(t);
	}
};

} // namespace

static_assert(pm::extract_bp::extractor_with_member<StaticPassThrough, int>);
static_assert(!pm::extract_bp::extractor_with_adl<StaticPassThrough, int>);
static_assert(pm::try_extract_bp::extractor_with_member<OptionalPassThrough, int>);
static_assert(!pm::try_extract_bp::extractor_with_adl<OptionalPassThrough, int>);

static_assert(pm::detail::is_extract<pm::detail::extract<StaticPassThrough, int>>);
static_assert(!pm::detail::is_extract<pm::detail::try_extract<OptionalPassThrough, int>>);
static_assert(pm::detail::is_try_extract<pm::detail::try_extract<OptionalPassThrough, int>>);
static_assert(!pm::detail::is_try_extract<pm::detail::extract<StaticPassThrough, int>>);

static_assert(pm::detail::is_unconditional_extract<pm::detail::extract<StaticPassThrough, int>>);
static_assert(!pm::detail::is_unconditional_extract<pm::detail::try_extract<OptionalPassThrough, int>>);
static_assert(pm::detail::is_conditional_extract<pm::detail::try_extract<OptionalPassThrough, int>>);
static_assert(!pm::detail::is_conditional_extract<pm::detail::extract<StaticPassThrough, int>>);

// tentative extract should satisfy extract/try_extract concept too
// to fit into is_conditional_extract/is_unconditional_extract
static_assert(pm::detail::is_unconditional_extract<
	pm::detail::tentative_extract<pm::detail::extract<StaticPassThrough, int>>>);
static_assert(!pm::detail::is_unconditional_extract<
	pm::detail::tentative_extract<pm::detail::try_extract<OptionalPassThrough, int>>>);
static_assert(pm::detail::is_conditional_extract<
	pm::detail::tentative_extract<pm::detail::try_extract<OptionalPassThrough, int>>>);
static_assert(!pm::detail::is_conditional_extract<
	pm::detail::tentative_extract<pm::detail::extract<StaticPassThrough, int>>>);

static_assert(pm::detail::has_matcher<pm::detail::extract<StaticPassThrough, int>, int>);
static_assert(pm::detail::has_matcher<pm::detail::extract<StaticPassThrough, int>, int &>);
static_assert(pm::detail::has_matcher<pm::detail::extract<StaticPassThrough, int>, const int &>);
static_assert(pm::detail::has_matcher<pm::detail::extract<StaticPassThrough, int>, int &&>);
static_assert(pm::detail::has_matcher<pm::detail::try_extract<OptionalPassThrough, int>, int>);
static_assert(pm::detail::has_matcher<pm::detail::try_extract<OptionalPassThrough, int>, int &>);
static_assert(pm::detail::has_matcher<pm::detail::try_extract<OptionalPassThrough, int>, const int &>);
static_assert(pm::detail::has_matcher<pm::detail::try_extract<OptionalPassThrough, int>, int &&>);

namespace {

template <typename T>
struct Is
{
	template <typename Arg>
	Arg&& extract(Arg&& arg) {
		static_assert(std::is_same_v<T, std::remove_cvref_t<Arg>>);
		return std::forward<Arg>(arg);
	}
};

template <typename T> // XXX: not quite the same definition as in P1371
using is = Is<T>;

template <typename T>
struct Copy
{
	template <typename Arg>
	T extract(const Arg& arg) {
		return T(arg);
	}
};

template <typename T>
using copy = Copy<T>;

} // namespace

void test_extract_unconditional()
{
	const auto f = [] {
		return 10;
	};

	assert(pm::match(f()
		| pm::with(pm::extract(is<int>{}),
			[] (int x) {
				assert(x == 10);
				return x;
			})));

	const auto g = [] {
		return std::string("abc");
	};

	assert(pm::match(g()
		| pm::with(pm::extract(is<std::string>{}),
			[] (const std::string &s) {
				assert(s == "abc");
				return s;
			})));

	// match c-string
	assert(pm::match(g().c_str()
		| pm::with(pm::extract(is<const char *>{}),
			[] (const std::string_view sv) {
				assert(sv == "abc");
				return sv;
			})));

	// make std::string copy of c-string when extracting
	assert(pm::match("cba"
		| pm::with(pm::extract(copy<std::string>{}),
			[] (const std::string &s) {
				assert(s == "cba");
				return s;
			})));
}

namespace {

struct Email
{
	std::optional<std::array<std::string_view, 2>>
	try_extract(std::string_view sv) {
		const auto regex = std::regex(R"(^([^@]+)@([^@]+)$)");
		if (std::cmatch match; std::regex_match(sv.data(), match, regex)) {
			return std::array{
				std::string_view(match[1].first, match[1].length()),
				std::string_view(match[2].first, match[2].length()),
			};
		}

		return std::nullopt;
	}
};

using email = Email;

struct PhoneNumber
{
	std::optional<std::array<std::string_view, 3>>
	try_extract(std::string_view sv) {
		const auto regex = std::regex(R"(^\+\d+?-(\d{3})-(\d{3})-(\d{4})$)");
		if (std::cmatch match; std::regex_match(sv.data(), match, regex)) {
			return std::array{
				std::string_view(match[1].first, match[1].length()),
				std::string_view(match[2].first, match[2].length()),
				std::string_view(match[3].first, match[3].length()),
			};
		}

		return std::nullopt;
	}
};

using phone_number = PhoneNumber;

} // namespace

void test_extract_conditional()
{
	const auto match_func = [] (const std::string &s) {
		return pm::match(s
			| pm::with(pm::try_extract(email{})
				+ pm::decons(),
				[] (std::string_view address, std::string_view domain) {
					assert(address == "noreply");
					assert(domain == "example.com");
					return true;
				})
			| pm::with(pm::try_extract(phone_number{}) % std::make_tuple("415", pm::__, pm::__)
				+ pm::decons(),
				[] (std::string_view area, std::string_view range, std::string_view code) {
					assert(area == "415");
					assert(range == "555");
					assert(code == "0000");
					return true;
				})
		);
	};

	assert(match_func("noreply@example.com"));
	assert(!match_func("example.com"));

	assert(match_func("+1-415-555-0000"));
	assert(!match_func("+1-416-555-0000"));
}

namespace {

struct String
{
	friend std::string extract(String&, int x) {
		return std::to_string(x);
	}
};

} // namespace

namespace std {

std::string extract(std::string &, int x)
{
	return std::to_string(x);
}

} // namespace std

void test_extract_unconditional_adl()
{
	const auto match_1 = pm::match(10
		| pm::with(pm::extract(String{}),
			[] (const std::string &s) { return s; }));
	assert(match_1 && *match_1 == "10");

	const auto match_2 = pm::match(11
		| pm::with(pm::extract(std::string{}),
			[] (const std::string &s) { return s; }));
	assert(match_2 && *match_2 == "11");
}

namespace {

struct Int
{
	friend std::optional<int> try_extract(Int &, std::string_view sv) {
		int result = 0;
		auto [p, errc] = std::from_chars(sv.begin(), sv.end(), result);
		return (errc == std::errc() ? std::make_optional(result) : std::nullopt);
	}
};

} // namespace

void test_extract_conditional_adl()
{
	const auto match_1 = pm::match("10"
		| pm::with(pm::try_extract(Int{}),
			[] (int x) { return x; }));
	assert(match_1 && *match_1 == 10);
}

void test_extract_mutable_extracted_value()
{
	const auto match_1 = pm::match(10
		| pm::with(pm::extract(String{}),
			[] (std::string &s) {
				s = "modified";
				return s;
			}));
	assert(match_1 && *match_1 == "modified");

	// things we do for c++
	int x = 11;
	pm::match(x
		| pm::with(pm::extract(is<int>{}),
			[] (int &x) { x += 1; }));
	assert(x == 12);
}

void test_extract_const_extracted_value()
{
	const int x = 11;
	const auto match = pm::match(x
		| pm::with(pm::extract(is<int>{}),
			[] (const int &x) { return x + 1; }));
	assert(match && *match == 12);
}

void test_extract_move()
{
	std::string s = "wtf";
	const auto match = pm::match(s
		| pm::with(pm::extract(is<std::string>{}),
			[] (std::string &s) {
				// move matched value to match result
				auto ss = std::move(s);
				return ss;
			}));
	assert(s.empty());
	assert(match && *match == "wtf");
}

void test_extract_move_with_cons()
{
	std::string s = "чзх";
	const auto match = pm::match(s
		| pm::with(pm::extract(is<std::string>{})
			+ pm::cons([] (auto &&s) -> decltype(auto) { return s; }),
			[] (std::string &s) {
				auto ss = std::move(s);
				return ss;
			}));
	assert(s.empty());
	assert(match && *match == "чзх");
}

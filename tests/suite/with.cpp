#include <string>
#include <pm/ext.hpp>
#include <pm/pm.hpp>

static_assert(pm::detail::is_clause<pm::detail::with<int, decltype([]{})>, int, true>);
static_assert(pm::detail::is_clause<pm::detail::with<int, decltype([]{})>, int, false>);

void test_with_condition()
{
	assert(pm::match(10 | pm::with(pm::__,
		[] (int x) { return x <= 11; },
		[] { return true; })));

	assert(!pm::match(10 | pm::with(pm::__,
		[] (int x) { return x >= 11; },
		[] { return true; })));
}

void test_with_cons_and_condition()
{
	assert(pm::match(10 | pm::with(pm::__
		+ pm::cons([] (int x) { return std::to_string(x); }),
		[] (const std::string &s) { return (s == "10"); },
		[] { return true; })));
}

void test_with_delegate_condition()
{
	const auto test = [] (int x) {
		return (x <= 11);
	};

	assert(pm::match(10
		| pm::with(pm::__, test, [] { return true; })));
	assert(pm::match(11
		| pm::with(pm::__, test, [] { return true; })));
	assert(!pm::match(12
		| pm::with(pm::__, test, [] { return true; })));
}

void test_with_condition_with_decons()
{
	assert(pm::match(std::make_tuple(10, std::string("abc"))
		| pm::with(std::tie(pm::__, pm::__)
			+ pm::decons(),
			[] (int x, const std::string &s) { // guard must take const references
				assert(x == 10);
				assert(s == "abc");
				return true;
			},
			[] (int x, std::string &s) {
				assert(x == 10);
				assert(s == "abc");
				return true;
			})));
}

void test_with_const_condition()
{
	int x = 10;
	assert(pm::match(x
		| pm::with(pm::__,
			[] (const int &) { return true; }, // must not accept non-const reference
			[] (int &) { return true; })));
}

#ifndef PM_EXT_ANY_OF_HPP
#define PM_EXT_ANY_OF_HPP

#include <cassert>
#include <tuple>
#include <pm/detail/concepts.hpp>
#include <pm/detail/matcher.hpp>

namespace pm {
namespace detail {

// `any_of` lives in detail namespace and match() is supposed
// to match any_of from detail namespace similarly to how
// match for `within` work on type from detail
template <typename ...Args>
	requires (sizeof...(Args) > 0)
struct any_of
{
	using explicit_variant_matching = std::true_type;

	using tuple_t = std::tuple<Args...>;

	explicit constexpr any_of(Args ...args)
	: args(std::make_tuple(args...))
	{}

	tuple_t args;
};

template <typename T>
concept is_any_of = requires (T t) {
	{ pm::detail::any_of{t} } -> std::same_as<T>;
};

// Checks that value can potentially match at least one of the any_of args
template <std::size_t I, typename Anyof, typename V, typename Tuple=typename Anyof::tuple_t>
	requires (detail::is_any_of<Anyof>
	&& I < std::tuple_size_v<std::remove_cvref_t<Tuple>>)
consteval bool can_match_any_of_value()
{
	using tuple_t = std::remove_cvref_t<Tuple>;

	if constexpr (detail::has_matcher<std::tuple_element_t<I, tuple_t>, V>) {
		return true;
	}

	if constexpr (I+1 < std::tuple_size_v<tuple_t>) {
		return can_match_any_of_value<I+1, Anyof, V>();
	}
	else {
		return false;
	}
}

/// Goes over args, first arg matched returns true
template <std::size_t I, typename V, typename Args>
	requires (detail::is_tuple<std::remove_cvref_t<Args>>
	&& I < std::tuple_size_v<std::remove_cvref_t<Args>>)
constexpr bool match_any_of(V &&v, const Args &args)
{
	const auto &arg = std::get<I>(args);

	if constexpr (detail::has_matcher<decltype(arg), V>) {
		if (detail::get_matcher(arg, v).match(arg, v)) {
			return true;
		}
	}

	if constexpr (I+1 < std::tuple_size_v<Args>) {
		return match_any_of<I+1>(v, args);
	}

	return false;
}

template <typename Pattern, typename V>
	requires (detail::is_any_of<std::remove_cvref_t<Pattern>>)
struct any_of_matcher
{
	bool matched = false;

	template <bool Strict=true>
	constexpr bool match(const Pattern &any_of, auto &&v)
		requires (!Strict || detail::can_match_any_of_value<0, Pattern, V>())
	{
		matched = match_any_of<0>(v, any_of.args);
		return matched;
	}

	constexpr decltype(auto) result([[maybe_unused]] const Pattern &p, auto &&v) const noexcept {
		assert(matched);
		return std::forward<decltype(v)>(v);
	}
};

/// any_of matcher
template <typename Pattern, typename V>
	requires (detail::is_any_of<std::remove_cvref_t<Pattern>>)
struct matcher<Pattern, V> : any_of_matcher<Pattern, V> {};

} // namespace detail

template <typename ...Args>
constexpr detail::is_any_of auto any_of(Args ...args)
{
	return detail::any_of(args...);
}

} // namespace pm

#endif // PM_EXT_ANY_OF_HPP

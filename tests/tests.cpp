#include <iostream>
#include <functional>

void test_values_to_values();
void test_values_strict();
void test_values_match();
void test_pointers_to_pointers();
void test_pointers_to_smart_pointers();
void test_pointers_match();
void test_types_to_types();
void test_types_to_values();
void test_types_match();
void test_tuple_to_tuple();
void test_tuples_match();
void test_tuples_match_array();
void test_tuples_match_pair();
void test_variant_to_values();
void test_variant_to_types();
void test_variant_to_variant();
void test_variant_visiting();
void test_variant_match();
void test_variant_match_mixed_type_value();
void test_variant_strict();

void test_with_condition();
void test_with_cons_and_condition();
void test_with_delegate_condition();
void test_with_condition_with_decons();
void test_with_const_condition();

void test_any();
void test_any_wildcard();
void test_any_with_any();
void test_maybe_optional();
void test_maybe_shared_ptr();
void test_maybe_callback();
void test_surely_pointer();
void test_surely_optional();
void test_surely_callback();
void test_any_of();
void test_any_of_singular();
void test_any_of_variant_values();
void test_any_of_variant_types();
void test_any_of_range();
void test_any_of_strict();
void test_within();
void test_within_pair();
void test_extract_unconditional();
void test_extract_conditional();
void test_extract_unconditional_adl();
void test_extract_conditional_adl();
void test_extract_mutable_extracted_value();
void test_extract_const_extracted_value();
void test_extract_move();
void test_extract_move_with_cons();

void test_cons();
void test_cons_no_args_callback();
void test_cons_with_maybe();
void test_cons_no_decons();
void test_cons_with_surely_any();
void test_cons_open_cons();
void test_cons_open_cons_with_surely_any();
void test_cons_with_cons();
void test_cons_open_decons();

void test_neg();
void test_neg_open_neg();
void test_neg_any_of();
void test_conj();
void test_open_conj();
void test_conj_conj();
void test_disj();
void test_open_disj();
void test_disj_disj();

void test_match_return();
void test_match_return_void();
void test_match_chaining();
void test_match_capture_matched();
void test_match_pass();
void test_match_double_pass();
void test_match_pass_always();
void test_match_pass_last();
void test_match_pass_chaining();
void test_match_pass_return_type();
void test_match_pass_callback();
void test_match_struct();
void test_match_arguments_basic();
void test_match_arguments_tuples();
void test_match_arguments_variant();
void test_match_arguments_any_of();

void test_case_enum_matching();
void test_case_mixed_value_const_matching();
void test_case_pair_matching();
void test_polymorphic_matching();
void test_polymorphic_callback();
void test_stdany_match_types();
void test_stdany_match_callback();

void test_cookbook_5_3_1();
void test_cookbook_5_3_2_1();
void test_cookbook_5_3_2_2__1();
void test_cookbook_5_3_2_2__3();
void test_cookbook_5_3_2_3();
void test_cookbook_5_3_2_5();
void test_cookbook_5_3_2_6();
void test_cookbook_5_4();
void test_cookbook_side_effect_propagation();

int main()
{
	const std::function<void()> tests[] = {
		// built-ins
		test_values_to_values,
		test_values_match,
		test_values_strict,
		test_pointers_to_pointers,
		test_pointers_to_smart_pointers,
		test_pointers_match,
		test_types_to_types,
		test_types_to_values,
		test_types_match,
		test_tuple_to_tuple,
		test_tuples_match,
		test_tuples_match_array,
		test_tuples_match_pair,
		test_variant_to_values,
		test_variant_to_types,
		test_variant_to_variant,
		test_variant_visiting,
		test_variant_match,
		test_variant_match_mixed_type_value,
		test_variant_strict,
		// more built-ins
		test_with_condition,
		test_with_cons_and_condition,
		test_with_delegate_condition,
		test_with_condition_with_decons,
		test_with_const_condition,
		// extensions
		test_any,
		test_any_wildcard,
		test_any_with_any,
		test_maybe_optional,
		test_maybe_shared_ptr,
		test_maybe_callback,
		test_surely_pointer,
		test_surely_optional,
		test_surely_callback,
		test_any_of,
		test_any_of_singular,
		test_any_of_variant_values,
		test_any_of_variant_types,
		test_any_of_range,
		test_any_of_strict,
		test_within,
		test_within_pair,
		test_extract_unconditional,
		test_extract_conditional,
		test_extract_unconditional_adl,
		test_extract_conditional_adl,
		test_extract_mutable_extracted_value,
		test_extract_const_extracted_value,
		test_extract_move,
		test_extract_move_with_cons,
		// extra extensions
		test_cons,
		test_cons_no_args_callback,
		test_cons_with_maybe,
		test_cons_no_decons,
		test_cons_with_surely_any,
		test_cons_open_cons,
		test_cons_open_cons_with_surely_any,
		test_cons_with_cons,
		test_cons_open_decons,
		// boolean
		test_neg,
		test_neg_open_neg,
		test_neg_any_of,
		test_conj,
		test_open_conj,
		test_conj_conj,
		test_disj,
		test_open_disj,
		test_disj_disj,
		// general
		test_match_return,
		test_match_return_void,
		test_match_chaining,
		test_match_capture_matched,
		test_match_pass,
		test_match_double_pass,
		test_match_pass_always,
		test_match_pass_last,
		test_match_pass_chaining,
		test_match_pass_return_type,
		test_match_pass_callback,
		test_match_struct,
		test_match_arguments_basic,
		test_match_arguments_tuples,
		test_match_arguments_variant,
		test_match_arguments_any_of,
		// cases
		test_case_enum_matching,
		test_case_mixed_value_const_matching,
		test_case_pair_matching,
		test_polymorphic_matching,
		test_polymorphic_callback,
		test_stdany_match_types,
		test_stdany_match_callback,
		// cookbook
		test_cookbook_5_3_1,
		test_cookbook_5_3_2_1,
		test_cookbook_5_3_2_2__1,
		test_cookbook_5_3_2_2__3,
		test_cookbook_5_3_2_3,
		test_cookbook_5_3_2_5,
		test_cookbook_5_3_2_6,
		test_cookbook_5_4,
		test_cookbook_side_effect_propagation,
	};

	for (const auto &test : tests) {
		std::invoke(test);
		std::cout << ".";
	}
	// TODO: std::format()
	std::cout << "\n" << std::size(tests) << " test(s) passed" << std::endl;

	return 0;
}
